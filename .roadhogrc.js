const path = require('path');
const svgSpriteDirs = [
  require.resolve('antd-mobile').replace(/warn\.js$/, ''), // antd-mobile 内置svg
  path.resolve(__dirname, 'src/my-project-svg-foler'),  // 业务代码本地私有 svg 存放目录
];
export default{
  "entry": "src/index.js",
  svgSpriteLoaderDirs : svgSpriteDirs ,
  extensions: ['.web.js', '.js', '.json', '.jsx'],
  "env": {
    "development": {
      "extraBabelPlugins": [
         "transform-runtime",
          "dva-hmr",
          ["import", [{ "libraryName": "antd-mobile", "libraryDirectory": "lib", "style": 'css'}, { "libraryName": "antd", "libraryDirectory": "lib", "style": 'css'}]]
      ]
    },
    "production": {
      "extraBabelPlugins": [
        "transform-runtime",
        ["import", [{ "libraryName": "antd-mobile", "libraryDirectory": "lib", "style": 'css'}, { "libraryName": "antd", "libraryDirectory": "lib", "style": 'css'}]]
      ],
      "publicPath":"./"
    }
  }
}

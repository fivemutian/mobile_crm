import provinces from '../models/provinces.json';
import cities from '../models/cities.json';
import areas from '../models/areas.json';
// import streets from '../models/streets.json';
// streets.forEach((street) => {
//   const matchStreets = areas.filter(area => area.code === street.parent_code)[0];
//   if (matchStreets) {
//     matchStreets.children = matchStreets.children || [];
//     matchStreets.children.push({
//       label: street.name,
//       value: street.code,
//     });
//   }
// });
areas.forEach((area) => {
  const matchCity = cities.filter(city => city.code === area.parent_code)[0];
  if (matchCity) {
    matchCity.children = matchCity.children || [];
    matchCity.children.push({
      label: area.name,
      value: area.name,
      children:area.children
    });
  }
});

cities.forEach((city) => {
  const matchProvince = provinces.filter(province => province.code === city.parent_code)[0];
  if (matchProvince) {
    matchProvince.children = matchProvince.children || [];
    matchProvince.children.push({
      label: city.name,
      value: city.name,
      children: city.children,
    });
  }
});

const options = provinces.map(province => ({
  label: province.name,
  value: province.name,
  children: province.children,
}));

export default options;
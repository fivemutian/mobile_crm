import React from 'react';
import {connect} from 'dva';
import PropTypes from 'prop-types';
import {Link} from 'dva/router';
import {Button,Icon, Breadcrumb,Input,Popconfirm} from 'antd';
function onDelete(key,e,_this){
    const data = _this.state.data.filter(item => item.key !== key);
    _this.setState({data });
}
export const PublicCustomer={
	url:'https://randomuser.me/api',
	columns:[{
	  title: '客户名称',
	  dataIndex: 'name',
	  sorter: true,
	  width: '20%',
	}, {
	  title: '电话',
	  dataIndex: 'phone',
	  // filters: [
	  //   { text: 'Male', value: 'male' },
	  //   { text: 'Female', value: 'female' },
	  // ],
	  width: '20%',
	}, {
	  title: '地址',
	  dataIndex: 'address',
	},{
		title:'客户所有者',
		dataIndex:'owner',
	}, {
	  title: '动作',
	  key: 'action',
	  render: (text, record) => (
	    <span>
	      <Link to={{pathname:'/app/newcustomer',query:{'current':2}}}>编辑{record.name}</Link>
	      <span className="ant-divider" />
	     	<Popconfirm title="确认删除吗?" onConfirm={(e) => onDelete(record.key, e,this)}>
	     		<a>删除</a>
	     	</Popconfirm>
	    </span>
	  ),
	}],
}
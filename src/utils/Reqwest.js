import {Toast} from 'antd-mobile';
import reqwest from 'reqwest';
// let a =null;
export const reqwest_get = (_url,_data,_then,_error,_this)=>{
	// let a =null;
	reqwest({
      url: window.host+_url,
      method: 'get',
      crossOrigin: true,
      type: 'json',
      data:_data,
      // contentType: 'application/json',
      // headers:{
      //   'Access-Control-Allow-Origin':'*'
      // }
    }).then(_then
    ).fail((err,msg)=>{
      Toast.offline('网络连接失败',0.6)
    });
    // return a;
}
// if(a!=a){
	// export a;
// }
export const reqwest_post = (_url,_data,_then)=>{
	reqwest({
      url: window.host+_url,
      method: 'post',
      crossOrigin: true,
      type: 'json',
      data:_data,
      // contentType: 'application/json',
      // headers:{
      //   'Access-Control-Allow-Origin':'*'
      // }
    }).then(_then
    ).fail((err,msg)=>{
      Toast.offline('网络连接失败',0.6)
    });
}
export const reqwest_url = (_url,_data,_method,_then,_error,_this)=>{
  // let a =null;
  reqwest({
      url: window.host+_url,
      method: _method,
      crossOrigin: true,
      type: 'json',
      data:_data,
      // contentType: 'application/json',
      // headers:{
      //   'Access-Control-Allow-Origin':'*'
      // }
    }).then(_then
    ).fail((err,msg)=>{
      Toast.offline('网络连接失败',0.6)
    });
    // return a;
}
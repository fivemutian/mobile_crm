export const New_case={
	"/hfive/customers":{"url":"/hfive/newcus","text":"新建"},
	"/hfive/clues":{"url":"/hfive/newcl","text":"新建"},
	"/hfive/orders":{"url":"/hfive/newor","text":"新建"},
	"/hfive/stores":{"url":"/hfive/newsroes","text":"新建"},
	"/hfive/users":{"url":"/hfive/newuse","text":"新建"},
	"/hfive/strategies":{"url":"/hfive/newstra","text":"新建"},
	"/hfive/account":{"url":"/hfive/newan","text":"新建"},
	"/hfive/Account":{"url":"/hfive/newan","text":"新建"},
	"/hfive/dealer":{"url":"/hfive/newapply","text":"关联"},
	"/hfive/Dealer":{"url":"/hfive/newapply","text":"关联"},
	"/hfive/pay":{"url":"/bank/list","text":"银行卡"},
	// "/bank/list":{"url":"/bank","text":"新增"},
};

export const New_edit={
	"/hfive/infocus":[{"url":"/hfive/editcus","text":"编辑"}],
	"/hfive/infocl":[{"url":"/hfive/editcl","text":"编辑"},{"url":"/hfive/clues","api":"/api/clues/","text":"删除"}],
	// "/hfive/orders":[{"url":"/hfive/newor","text":"编辑"}],
	"/hfive/infosroes":[{"url":"/hfive/editsroes","text":"编辑"}],
	"/hfive/infouse":[{"url":"/hfive/users","text":"删除","api":"/api/users/"}],
	"/hfive/infostra":[{"url":"/hfive/editstra","text":"编辑"}],
	// "/hfive/account":{"url":"/hfive/newan","text":"编辑"},
	// "/hfive/dealer":{"url":"/hfive/newapply","text":"删除"},
	// "/hfive/Dealer":{"url":"/hfive/newapply","text":"删除"}
};
export const role_case = {
	"admin":"管理员",
	"saler":"销售",
	"saler_director":"销售主管",
	"cs":"客服",
	"acct":"财务",
	"introducer":"介绍人"
};
export const numvail= function(rule, value, callback){
	let phoneReg =/^1[34578]\d{9}$/g;
	if(!value){
		callback('请输入手机号码');
	}else if(!phoneReg.test(value)){
		callback('手机格式不正确');
	}
	console.log(value)
	callback()
}

// export const Num_Vail = numvail()]];
export const selectvail = function(rule,value,callback,mess){
	if(!value){
		callback(mess);
	}
	callback()
}
export const emailvail = function(rule, value, callback){
	let emailreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	if(!value){
		callback('请输入邮箱')
	}else if(!emailreg.test(value)){
		callback('请输入正确的邮箱格式');
	}
	callback();
}

export const mathvail = function(rule,value,callback){
	let _reg=/^[0-9]+(.[0-9]{1,3})?$/;
	if(!_reg.test(value)){
		callback('请输入数字')
	}else if(!value){
		callback(rule.message);
	}
	callback();
}
export const  bankvail  = (rule,value,callback)=>{
	let _reg=/^(\d{16}|\d{19})$/;
	if(!_reg.test(value)){
		callback('请输入正确的银行卡号')
	}else if(!value){
		callback('银行卡号不能为空');
	}
	callback();
}

let date_value=null;
export const datevali = function(value) {
	date_value = new Date(value);
	date_value = date_value.getFullYear()+'-'+(date_value.getMonth()+1<10?'0'+(date_value.getMonth()+1):date_value.getMonth()+1) + '-' +date_value.getDate();
	return date_value;
}
export const datevali_deta = function(value) {
	date_value = new Date(value);
	date_value = date_value.getFullYear()+'-'+(date_value.getMonth()+1<10?'0'+(date_value.getMonth()+1):date_value.getMonth()+1) + '-' +date_value.getDate()+' '+(date_value.getHours()<10?'0'+(date_value.getHours()):date_value.getHours())+':'+(date_value.getMinutes()<10?'0'+(date_value.getMinutes()):date_value.getMinutes());
	return date_value;
}
import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination } from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../../style/App.less'
import {Button,Input, Progress} from 'antd'
import reqwest from 'reqwest';
class Content extends React.Component{
	constructor(props){
		super(props);
		this.state={

		}
	}
	render(){
			const sidebar = (<List>
      {[...Array(2).keys()].map((i, index) => {
        if (index === 0) {
          return (<List.Item key={index}
            thumb="https://zos.alipayobjects.com/rmsportal/eOZidTabPoEbPeU.png"
            multipleLine
          >Category</List.Item>);
        }
          return (<List.Item key={index}
       	   	thumb="https://zos.alipayobjects.com/rmsportal/eOZidTabPoEbPeU.png"
        	>Category{index}</List.Item>);
      	})}
   	  </List>);
		 const { getFieldProps } = this.props.form;
		 return(
			<div style={{height:'100%'}}>
				<NavBar style={{height:'1rem',background:'#3dbd7d'}} iconName="ellipsis" onLeftClick={this.onOpenChange.bind(this)}>管家快销</NavBar>
	      <Drawer
	        className={`${style.my_drawer}`}
	        style={{ minHeight: document.documentElement.clientHeight - 200, }}
	        dragHandleStyle={{ display: 'none' }}
	        contentStyle={{ color: '#A6A6A6', textAlign: 'left', }}
	        sidebar={sidebar}
	        {...drawerProps}
	      >
	       {this.props.children}
	      </Drawer>
	    </div>
	 	)
		
	}
}
const ContentForm = createForm()(Content);

export default connect()(ContentForm)
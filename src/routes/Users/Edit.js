import React from 'react';
import { connect } from 'dva';
import { Picker,Radio, List, NavBar,Icon, InputItem , TabBar,DatePicker ,Toast ,Button,TextareaItem,WhiteSpace,Flex,Checkbox} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
import {range,disabledDate,disabledDateTime } from '../../utils/Date.js'
import moment from 'moment';
import 'moment/locale/zh-cn';
const RadioItem = Radio.RadioItem;
const CheckboxItem = Checkbox.CheckboxItem;
const AgreeItem = Checkbox.AgreeItem;
const maxTime = moment('22:00 +0800', 'HH:mm Z').utcOffset(8);
const minTime = moment('08:30 +0800', 'HH:mm Z').utcOffset(8);
class OrderEdit extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
      value:'saler_director',
		}
	}
	loadServer(params={}){
		console.log(params)
		const _this = this;
		reqwest({
      url: window.host+'/api/users',
      method: 'post',
      crossOrigin: true,
      type: 'json',
      data:{  access_token:localStorage['token'],
        user:{name:params.contact,mobile:params.phone,password:params.password}
      }
    }).then((data) => {
      if(data.status=='success'){
          Toast.success(
          '保存成功',
          1,
          ()=>{
            window.location.hash='/hfive/users';
          }
        )
        
      }else{
        Toast.fail(
          data.msg,
          1
        )
      }
    }).fail( (err, msg) =>{
       Toast.offline('网络连接失败!!!', 1);
    });
	}
	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
        // console.log(JSON.parse(values.material[0]));
      if (!err  ) {
        console.log('Received values of form: ', values);
      	_this.loadServer(values)
      }
    });
	}
	componentWillMount(){
    reqwest({
      url: window.host+'/api/sync/accounts/get_saler_directors',
      method: 'get',
      crossOrigin: true,
      type: 'json',
      data:{actoken:window.localStorage["token"]},
      // contentType: 'application/json',
      // headers:{
      //   'Access-Control-Allow-Origin':'*'
      // }
    }).then((data) => {
       
          this.setState({
            saler:data.list
          })

    }).fail((err,msg)=>{
      console.log(err)
    });
    if(window.sessionStorage["ism"]==undefined){
      window.location.hash='/hfivce/users'
    }else{
      this.setState({ism:JSON.parse(window.sessionStorage["ism"])})
    }
	}
  onChange(value,i){
    console.log(i);
    this.setState({radio:i.value})
  }
	render(){
		console.log(this.state.list);
    const {value,value2,ism} =this.state;
		const { getFieldProps , getFieldError} = this.props.form;
		const _this =this;
    const _companies =[];
    const _material=[];
    let saler_dir=null;
    console.log(this.state.saler)
    // const _slaer = this.state.saler.map(function(data){
    //   console.log(data)
    // })
    const data = [
      { value: 'saler_director', label: '销售主管' },
      { value: 'saler', label: '销售' },
      { value: 'cs', label: '客服' },
      { value: 'acct', label: '财务' },
    ];
    const data2 = [
      { value: 0, label: 'Basketball', extra: 'Details' },
      { value: 1, label: 'Football', extra: 'Details' },
    ];
    if(this.state.radio=='saler'){
      saler_dir = <List renderHeader={() => '选择你的销售主管'}>
        
      </List>
    }
		return(
			<div className={`${style.layout}`}>
				<List className={`${Reset.tel_list_header}`} renderHeader={() => '创建用户'}>
          <InputItem
	          clear
            className={`${Reset.tel_input}`}
	          error={!!getFieldError('phone')}
	          onErrorClick={() => {
	            alert(getFieldError('phone').join('、'));
	          }}
            {...getFieldProps('phone',{
            	initialValue:ism.mobile,
            	rules:[{required:true,message:'请输入手机号码',len:11,validator:(rule,value,callback)=>{
								!!value?numvail(rule,value.replace(/\s+/g, ""),callback):callback('请输入手机号码')
							}}]
            })}
            type='phone'
            placeholder="手机号码"
          >手机号码</InputItem>
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('password')}
            onErrorClick={() => {
              alert(getFieldError('password').join('、'));
            }}
            {...getFieldProps('password',{
              rules:[{required:true,message:'请输入手机号码',len:11,validator:(rule,value,callback)=>{
                !!value?numvail(rule,value.replace(/\s+/g, ""),callback):callback('请输入手机号码')
              }}]
            })}
            type='password'
            placeholder="*****"
          >密码</InputItem>
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('contact')}
            onErrorClick={() => {
              alert(getFieldError('contact').join('、'));
            }}
            {...getFieldProps('contact',{
              rules:[{required:true,message:'姓名'}]
            })}
            placeholder="姓名"
          >联系人</InputItem>
         
        </List>
        <List className={`${Reset.tel_list_header}`} renderHeader={() => '选择角色'}>
          <Flex>
            <Flex.Item>
              {data.map((i,index) => (
                <AgreeItem  key={i.value} data-seed={index} checked={i.value === this.state.radio} className={`${Reset.label} ${Reset.tel_radio}`} onChange={(e) =>{this.onChange(index,i) }}>
                  {i.label}
                </AgreeItem>
              ))}
            </Flex.Item>
          </Flex>
          
           
        </List>

        <Button className={`${Reset.btn_moblie}`} onClick={this.handleSubmit.bind(this)}  type='primary'>保存</Button>
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}

const OrderForm = createForm()(OrderEdit);
export default connect()(OrderForm)
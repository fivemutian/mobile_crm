import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
import {role_case} from '../../utils/New.js'
const Item = List.Item;
const Brief = Item.Brief;
class Account extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false
		}
	}
	loadServer(params={} ) {
    // alert(params);
    let _data=[];
    let _this=this;
    this.setState({ animating: true });
    reqwest({
      url: window.host+'/api/users',
      method: 'get',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        page:params,
        pre:5
      },
      type: 'json',
    }).then((data) => {
      // alert(data.status);
      const pagination = { ...this.state.pagination };
      pagination.total = data.total;
      this.setState({
        animating: false,
        // data: _data,
        list:data.list,
        pagination:pagination.total,
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
  }
  componentWillMount(){
  	this.loadServer(1)
    if(JSON.parse(window.localStorage["setting"]).role=='admin'){
      window.sessionStorage['new']='/hfive/newuse';
      window.sessionStorage['menu_edit']=JSON.stringify([{"url":"del","text":"删除","method":"/api/users/"}]);
      this.setState({aaa:'aa'})
    }
  }
  onDelete(){
    reqwest({
        url: window.host+'/api/users/'+key,
        method: 'delete',
        crossOrigin: true,
        data: {
          access_token:localStorage['token'],
        },
        type: 'json',
      }).then((data) => {
        console.log(data);
        if(data.status=='success'){
          const pagination = { ...this.state.pagination };
          message.success('删除成功',0.1)
          _this.fetch({page:1});
        }else{
          message.warning(data.msg,0.1)
        }
        // Read total count from server
        // pagination.total = data.totalCount;
          
      });
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)

  }
  componentWillUnmount(){
    
  }
	render(){
		const ItemList = this.state.list.map(function(data){
			return <Link key={data.id} to={{pathname:'/hfive/infouse'}} onClick={()=>{window.sessionStorage["ism"]=JSON.stringify(data)}}><Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        platform="android"
        className={`${Reset.tel_list}`}
      >
        姓名:{data.name}<Brief>电话:{data.mobile}</Brief>
        <Brief>用户分类:{role_case[data.role]}</Brief>
      </Item></Link>
		})
		return(
			<div className="flex-container">
        <List renderHeader={() => '账号管理'}  className={`${Reset.tel_list_header}`}>
      	  {ItemList}
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem',marginBottom:'0.7rem'}} total={Math.ceil(this.state.pagination/5)} 
		      locale={{
		        prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
		        nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
		      }} />
			</div>
		)
	}
}
export default connect()(Account)
import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Card,TarBar} from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
import {role_case} from '../../utils/New.js'
const Item = List.Item;
const Brief = Item.Brief;
class StoresLink extends React.Component{
  constructor(props){
    super(props);
    this.state={
      list:[],
      pageCurrent:0,
      animating:false,
      ism:null
    }
  }
  componentWillMount(){
    // alert(localStorage['token'])
    // if(window.location.host=="172.16.0.249:8181"){
    //  // sessionStorage['token']="9aySPxzAmrO2xwg/PKFFKyOR/Tz3AB7OsmxH2kRUn/S2X3HFwtv386zl3gRB8nOkUdo4zUsqepccrCe1BgPrTA=="
    // }
    if(window.sessionStorage["ism"]==undefined){
      window.location.hash='/hfivce/users'
    }else{
      this.setState({ism:JSON.parse(window.sessionStorage["ism"])})
    }
    // if(JSON.parse(window.sessionStorage["setting"]).role=='admin'){
    	// window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/edituse","text":"编辑"},{"url":"del","text":"删除"}]);
    	this.setState({aaa:'aa'})
    // }
  }
  handelPageChage(e,tar){
    // this.setState({pageCurrent:e})
    // this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillUnmount(){
    console.log(1);
    // window.sessionStorage.removeItem('bianji'); 
  }
  render(){
    const {ism} = this.state;
    return(
      <div className="flex-container">
        <List renderHeader={() => '用户信息'}  className={`${style.infor}`}>
          <Brief><span>姓名:</span><span>{ism.name}</span></Brief>
          <Brief><span>电话:</span><span>{ism.mobile}</span></Brief>
          <Brief><span>用户分类:</span><span>{role_case[ism.role]}</span></Brief>
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <List renderHeader={() => '基础信息'}  className={`${style.infor}`}>
        </List>

      </div>
    )
  }
}
export default connect()(StoresLink)
import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace,ImagePicker} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import PayStyle from './Pay.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
import {reqwest_post,reqwest_get} from '../../utils/Reqwest.js';
const Item = List.Item;
const Brief =  List.Item.Brief;
class CustomerCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
      data:[],
      pageCurrent:0,
			set:JSON.parse(window.localStorage['setting']),
			files:[],
			filesList:[],
      list:[],
      pagination:0,
      wallet:{amount:0,income:0,expend:0}
		}
	}
	onChange (files, type, index) {
    console.log(files, type, index);
    this.setState({
      files,
    });
  }
  getWallet(current){
    let _this = this;
    reqwest_get('/api/wallets/me',{
        access_token:localStorage['token'],
        page:current
      },(data) => {
        console.log(data);
        _this.setState({wallet:data.wallet,data:data.list,pagination:data.total});
        // console.log(this.state.wallet)
     })
  }
  handleImageClick(index,fs){
  	this.onChange.bind(this);
  }
  handelPageChage(e,tar){
    console.log(e)
    this.setState({pageCurrent:e})
    this.getWallet(e+1);
    // console.log(tar)
  }
	componentWillMount(){
    this.getWallet()
	}
	render(){
		console.log(this.state.list)
			const { set} =this.state;
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
     const ItemList = this.state.data.map(function(data){
      return <Item key={Math.random()*1} 
        multipleLine
        className={`${Reset.tel_list}`}
        onClick={() => {}}
        platform="android"
      ><Link  >
        <span style={{color:'#000'}}>时间：{data.time}</span><Brief>操作：{data.transfer}&ensp;<a>金额：{data.amount}</a></Brief>
      </Link></Item>
    })
		 return(
			<div className={`${style.layout}`}>
        <div className={`${PayStyle.pay}`}>
          <span>
            <span>我的金额</span><span>{_this.state.wallet.amount}</span>
          </span>
          <span>
            <span>总收入</span><span>{_this.state.wallet.income}</span>
          </span>
          <span>
            <span>总提成</span><span>{_this.state.wallet.expend}</span>
          </span>
        </div>
				<List  className={`${Reset.tel_list_header}`} renderHeader={() => '历史'}>
          {this.state.data.length<1?<Item
              multipleLine
              onClick={() => {}}
              platform="android"
              className={`${style.dislike}`}
            >
              <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
              <Brief>暂无数据</Brief>
           </Item>:ItemList}
     	  </List>
        <Pagination  onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem',marginBottom:'1rem'}} total={Math.ceil(this.state.pagination/10)} 
            locale={{
              prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
              nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
            }} />
        <WhiteSpace size='xl' />
        <WhiteSpace size="sm" />
          <div className={`${style.tab}`}>
            <Link to={{pathname:'recharge'}}>充值</Link>
            <Link className={`${style.white}`} style={{background:'#108ee9',color:'#fff'}} to={{pathname:'withdra'}}>提现</Link>
          </div>
			</div>
	 	)
		
	}
}
const CustomerForm = createForm()(CustomerCreate);
export default connect()(CustomerForm)
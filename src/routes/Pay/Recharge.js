import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import { Drawer, List, NavBar ,Icon,Modal,WhiteSpace,InputItem,Popup,Button,Toast} from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../../style/App.less'
import NavStyle from '../../style/ResNav.less'
import Reset from '../../style/ResetInput.less'
import pingpp from 'pingpp-js'
import reqwest from 'reqwest'
import {reqwest_get,reqwest_post} from '../../utils/Reqwest.js';
const ListItem = List.Item;
const isIPhone = new RegExp('\\biPhone\\b|\\biPod\\b', 'i').test(window.navigator.userAgent);
let maskProps;
if (isIPhone) {
  maskProps = {
    onTouchStart: e => e.preventDefault(),
  };
}
class Recharge extends React.Component{
	constructor(props){
		super(props);
		this.state={
			dacked:false,
			open: false,
      position: 'left',
      visible:false,
      prevPathName:'',
      edit:'null',
      url:'/home',
      sel: '',
		}
	}
	onDock(d){
		this.setState({
      [d]: !this.state[d],
    });
	}
	 onOpenChange = (...args) => {
    console.log(args);
    this.setState({ open: !this.state.open });
    this.setState({url:window.sessionStorage['new']})
  }
  config(){
    let _this =this;
  	reqwest({
      url: window.host+'/api/wallets/config',
      method: 'get',
      crossOrigin: true,
      // headers:AUTH_KEY,
      // contentType: 'application/json',
      data:{
        access_token:window.localStorage['token']
      },
      type: 'json',
    }).then((data) => {
      console.log(data);
      // sessionStorage['companies']=JSON.stringify(data.companies);
      // sessionStorage['material']=JSON.stringify(data.libs);
      // this.setState({store_tree:data.stores_tree})
   		window.localStorage['appId']=data.config.appId;
      _this.setState({timestamp:data.config.timestamp})
      window.localStorage['nonceStr']=data.config.nonceStr;
      window.localStorage['signature']=data.config.signature;
      wx.config({
		    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
		    appId:data.config.appId, // 必填，公众号的唯一标识
		    timestamp: data.config.timestamp, // 必填，生成签名的时间戳
		    nonceStr: data.config.nonceStr, // 必填，生成签名的随机串
		    signature: data.config.signature,// 必填，签名，见附录1
		    jsApiList: ['onMenuShareTimeline',
'onMenuShareAppMessage',
'onMenuShareQQ',
'onMenuShareWeibo',
'onMenuShareQZone',
'startRecord',
'stopRecord',
'onVoiceRecordEnd',
'playVoice',
'pauseVoice',
'stopVoice',
'onVoicePlayEnd',
'uploadVoice',
'downloadVoice',
'chooseImage',
'previewImage',
'uploadImage',
'downloadImage',
'translateVoice',
'getNetworkType',
'openLocation',
'getLocation',
'hideOptionMenu',
'showOptionMenu',
'hideMenuItems',
'showMenuItems',
'hideAllNonBaseMenuItem',
'showAllNonBaseMenuItem',
'closeWindow',
'scanQRCode',
'chooseWXPay',
'openProductSpecificView',
'addCard',
'chooseCard',
'openCard'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
		});
		wx.ready(function(){
		    // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
		    wx.onMenuShareTimeline({
			    title: 'wudi', // 分享标题
			    link: 'http://1.iujessica.applinzi.com/weixin_bang.php#/recharge?_k=1qhkcm', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
			    imgUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1498716635&di=e6b82d95fd53b17355d329f641643787&imgtype=jpg&er=1&src=http%3A%2F%2Fimage1.92bizhi.com%2Fart_android-3-0_03-1440x900.jpg', // 分享图标
			    success: function () { 
			    	console.log(333)
			        // 用户确认分享后执行的回调函数
			    },
			    cancel: function () { 
			        // 用户取消分享后执行的回调函数
			    }
			});
		    // wx.checkJsApi({
		    // jsApiList: ['onMenuShareTimeline','chooseWXPay'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
		    // success: function(res) {
		    // 	alert(3333)
		    //     // 以键值对的形式返回，可用的api值true，不可用为false
		    //     // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
		    // },error:function(res){
		    // 	console.log(res)
		    // }
		// });
		// });
		// wx.error(function(res){
		//     // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
		//     alert(2)
		});
	
	
		
    }).fail( (err, msg) =>{
       console.log(1)
       // this.setState({loading:true});
    })
    // this.weChatPay()
  	console.log(wx);
		
  }
  componentWillMount(){
    console.log(navigator.platform)
    let _menu=[{title:'主页',url:'home'}];
    // this.config()

    // this.recharge();
  }
  recharge(){
    let _this = this;
    reqwest({
        url: window.host+'/api/wallets/recharge',
        method: 'post',
        crossOrigin: true,
        // headers:AUTH_KEY,
        // contentType: 'application/json',
        data:{
          access_token:window.localStorage['token'],
          amount:_this.state.recharge,
          trade_type:'alipay_pc_direct'
        },
        type: 'json',
      }).then((data)=>{
           if(data.status=='success'){
         pingpp.createPayment(data.data.charge, function(result, err){
           // alert(result);
           if (result == "success") {
             // 只有微信公众账号 wx_pub 支付成功的结果会在这里返回，其他的支付结果都会跳转到 extra 中对应的 URL。

             console.log(1)
           } else if (result == "failed") {
              Toast.fail(data.msg,0.6)
             // charge 不正确或者微信公众账号支付失败时会在此处返回
           } else if (result == "cancel") {
             // 微信公众账号支付取消支付
             Toast.fail('取消支付',0.6)
           }
         });
        }
      }).fail((data)=>{

      })
  }
  // weChatPay(){
  //    WeixinJSBridge.invoke(
  //        'getBrandWCPayRequest', {
  //            "appId":"wx2421b1c4370ec43b",     //公众号名称，由商户传入     
  //            "timeStamp":"1395712654",         //时间戳，自1970年以来的秒数     
  //            "nonceStr":"e61463f8efa94090b1f366cccfbbb444", //随机串     
  //            "package":"prepay_id=u802345jgfjsdfgsdg888",     
  //            "signType":"MD5",         //微信签名方式：     
  //            "paySign":"70EA570631E4BB79628FBCA90534C63FF7FADD89" //微信签名 
  //        },
  //        function(res){     
  //            if(res.err_msg == "get_brand_wcpay_request:ok" ) {}     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。 
  //        }
  //    ); 
  //    if (typeof WeixinJSBridge == "undefined"){
  //    if( document.addEventListener ){
  //        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
  //    }else if (document.attachEvent){
  //        document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
  //        document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
  //    }
  //   }else{
  //      onBridgeReady();
  //   }
  // }

  rechargeWe(){
    let _data =new Date().getTime()
  	// let _this = this;
  	// reqwest({
   //    url: window.host+'/api/wallets/recharge',
   //    method: 'post',
   //    crossOrigin: true,
   //    // headers:AUTH_KEY,
   //    // contentType: 'application/json',
   //    data:{
   //      access_token:window.localStorage['token'],
   //      amount:_this.state.recharge,
   //      trade_type:'wx_pub'
   //    },
   //    type: 'json',
   //  }).then((data) => {
      // console.log(data);
      WeixinJSBridge.invoke(
       'getBrandWCPayRequest', {
           "appId":"wx2421b1c4370ec43b",     //公众号名称，由商户传入     
           "timeStamp":"1395712654",         //时间戳，自1970年以来的秒数     
           "nonceStr":"e61463f8efa94090b1f366cccfbbb444", //随机串     
           "package":"prepay_id=u802345jgfjsdfgsdg888",     
           "signType":"MD5",         //微信签名方式：     
           "paySign":"70EA570631E4BB79628FBCA90534C63FF7FADD89" //微信签名 
       },
       function(res){     
           if(res.err_msg == "get_brand_wcpay_request:ok" ) {}     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。 
       }
   ); 
      // 	wx.chooseWXPay({
		    // // timestamp: data.data.charge.credential.wx_pub.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
		    // // nonceStr: data.data.charge.credential.wx_pub.nonceStr, // 支付签名随机串，不长于 32 位
		    // // package: data.data.charge.credential.wx_pub.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
		    // // signType: 'MD5', // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
		    // // paySign: data.data.charge.credential.wx_pub.paySign, // 支付签名
      //    // "appId":"wx1211e96e6c279899",     //公众号名称，由商户传入     
      //        "timestamp":this.state.timestamp,         //时间戳，自1970年以来的秒数     
      //        "nonceStr":"e61463f8efa94090b1f366cccfbbb444", //随机串     
      //        "package":"prepay_id=wx212312312312313",     
      //        "signType":"MD5",         //微信签名方式：     
      //        "paySign":"70EA570631E4BB79628FBCA90534C63FF7FADD89" ,//微信签名,
		    // success: function (res) {
		    //     // 支付成功后的回调函数
		    //     console.log(res)
		    // },
		    // fail:function(res){
		    // 	alert(JSON.stringify(res));
		    // }
		// });
      // reqwest_post('/api/wallets/recharge',{access_token:window.localStorage['token'],amount:this.state.recharge,trade_type:'wx_pub'},(data)=>{
      //   alert(JSON.stringify(data));
      // })
   //    if(data.status=='success'){
			// 	pingpp.createPayment(data.data.charge, function(result, err){
			// 		alert(result);
			// 	  if (result == "success") {
			// 	    // 只有微信公众账号 wx_pub 支付成功的结果会在这里返回，其他的支付结果都会跳转到 extra 中对应的 URL。
			// 	    console.log(1)
			// 	  } else if (result == "fail") {
			// 	    // charge 不正确或者微信公众账号支付失败时会在此处返回
			// 	  } else if (result == "cancel") {
			// 	    // 微信公众账号支付取消支付
			// 	  }
			// 	});
			// }
    // }).fail( (err, msg) =>{
    //    console.log(1)
    //    // this.setState({loading:true});
    // })
  	// console.log(wx);
  }
  componentsDidMount(){
    
  }
  onClose = (sel) => {
    this.setState({ sel });
    Popup.hide();
  };
  handleClick(){
  	Popup.show(<div>
      <List className={`${Reset.tel_list_header}`} renderHeader={() => (
        <div style={{ position: 'relative' ,fontSize:'0.28rem'}}>
          委托买入
          <span
            style={{
              position: 'absolute', right: 3, top: -5,
            }}
            onClick={() => this.onClose('cancel')}
          >
            <Icon type="cross" style={{width:'0.44rem',height:'0.44rem'}} />
          </span>
        </div>)}
        className="popup-list"
      >
        {['更多', '更多',  '更多', '更多'].map((i, index) => (
          <List.Item className={`${Reset.tel_list}`} key={index}>{i}</List.Item>
        ))}
      </List>
      <ul style={{ padding: '0.18rem 0.3rem', listStyle: 'none' }}>
        <li>投资说明投资说明...</li>
        <li style={{ marginTop: '0.18rem' }}>
          <Button className={`${Reset.btn_moblie} ${Reset.tel_btn}`} type="primary" onClick={() => this.onClose('cancel')}>买入</Button>
        </li>
      </ul>
    </div>, { animationType: 'slide-up', maskProps, maskClosable: false });
  }
  handleClickRecharge(){

  }
	render(){
    let _edit=null;
    let offsetX = -10; // just for pc demo
    if (/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)) {
      offsetX = -26;
    }
    const drawerProps = {
      open: this.state.open,
      position: this.state.position,
      onOpenChange: this.onOpenChange.bind(this),
    };

		return(
			<div style={{height:'100%'}}>
			<NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/pay'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>}  iconName={false} rightContent={_edit}  >充值</NavBar>
				<WhiteSpace size='lg' />
 			
 			<WhiteSpace size='xl' />
 				<InputItem placeholder='充值金额' onChange={(e)=>{this.setState({recharge:e})}} type='money' className={`${Reset.tel_input}`} >
 					金额
 				</InputItem>
      <Button className={`${Reset.btn_moblie} ${Reset.tel_btn}`} type="primary" onClick={() => this.recharge()}>充值</Button>
    </div>
		)
	}
}
export default connect()(Recharge);
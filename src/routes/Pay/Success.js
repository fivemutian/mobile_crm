import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Card,Button} from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import NavStyle from '../../style/ResNav.less'
import PayStyle from './Pay.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
class Success extends React.Component{
  constructor(props){
    super(props);
    this.state={
      list:[],
      pageCurrent:0,
      animating:false,
      // ism:JSON.parse(window.sessionStorage['ism'])
    }
  }
  componentWillMount(){
    // alert(localStorage['token'])
    // if(window.location.host=="172.16.0.249:8181"){
    //  // sessionStorage['token']="9aySPxzAmrO2xwg/PKFFKyOR/Tz3AB7OsmxH2kRUn/S2X3HFwtv386zl3gRB8nOkUdo4zUsqepccrCe1BgPrTA=="
    // }
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editcus","text":"编辑"},{"url":"del","text":"删除"}]);
    // this.setState({aaa:'aa'})
  }
  handelPageChage(e,tar){
    // this.setState({pageCurrent:e})
    // this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillUnmount(){
    console.log(1);
  }
  render(){
    const {ism} = this.state;
    return(
      <div className="flex-container"> 
        <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/pay'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>}  iconName={false}  >充值结果</NavBar>
        <div className={`${PayStyle.success}`}>
          <span><Icon type={require('!svg-sprite!../../assets/Path/true.svg')} />充值成功</span>
        </div>
        <WhiteSpace size='lg' />
        <List className={`${style.infor} ${PayStyle.success_center}`}>
          <Brief>付款金额:<a>123</a></Brief>
          <Brief>付款金额:<a>123</a></Brief>
        </List>
        <WhiteSpace size='lg' />
        <Button >查看订单信息</Button>
        <WhiteSpace size='lg' />
        <Button >重新充值</Button>

      </div>
    )
  }
}
export default connect()(Success)
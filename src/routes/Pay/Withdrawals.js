import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import { Drawer, List, NavBar ,Icon,Modal,WhiteSpace,InputItem,Popup,Button,Toast} from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../../style/App.less'
import NavStyle from '../../style/ResNav.less'
import Reset from '../../style/ResetInput.less'
import PayStyle from './Pay.less'
// import reqwest from 'reqwest'
import pingpp from 'pingpp-js'
import {reqwest_post,reqwest_get} from '../../utils/Reqwest.js';
const ListItem = List.Item;
const isIPhone = new RegExp('\\biPhone\\b|\\biPod\\b', 'i').test(window.navigator.userAgent);
let maskProps;
if (isIPhone) {
  // Note: the popup content will not scroll.
  maskProps = {
    onTouchStart: e => e.preventDefault(),
  };
}
class Withdrawals extends React.Component{
	constructor(props){
		super(props);
		this.state={
			dacked:false,
			open: false,
      position: 'left',
      visible:false,
      prevPathName:'',
      edit:'null',
      url:'/home',
      sel: '',
      bank_num:null,
      bank_cards:[]
		}
	}
	onDock(d){
		this.setState({
      [d]: !this.state[d],
    });
	}
	onOpenChange = (...args) => {
    console.log(args);
    this.setState({ open: !this.state.open });
    this.setState({url:window.sessionStorage['new']})
  }
  componentWillMount(){
    console.log(navigator.platform)
    this.getBankCard();
    // let _menu=[{title:'主页',url:'home'}];
  }
  componentsDidMount(){
    // if(!!this.props.location.pathname.match(/info/g)){
    //   this.setState({edit:1})
    // }else if(!!this.props.location.pathname.match(/edit/g)){
    //   this.setState({edit:'edit'})
    // }else{
    //   this.setState({edit:'create'})
    // }
    
  }
  onClose = (sel) => {
    this.setState({ bank_num:sel });
    Popup.hide();
  };
  onWithdrawOk(){
    let _this = this;
    this.props.form.validateFields((err, values) => {
      console.log(values);
      if (!err) {
        reqwest_post('/api/wallets/withdraw',{
          access_token:window.localStorage['token'],
          amount:values.withdraw,
          bank_card_id:_this.state.bank_num.id
        },(data)=>{
          if(data.status=='success'){
            Toast.success(data.msg,0.6);
           // pingpp.createPayment(data.data.charge, function(result, err){
           //   // alert(result);wx_
           //   if (result == "success") {
           //     // 只有微信公众账号 wx_pub 支付成功的结果会在这里返回，其他的支付结果都会跳转到 extra 中对应的 URL。
           //     console.log(1)
           //   } else if (result == "fail") {
           //     // charge 不正确或者微信公众账号支付失败时会在此处返回
           //   } else if (result == "cancel") {
           //     // 微信公众账号支付取消支付
           //   }
           // });
          }else{
            Toast.fail(data.msg,0.5)
          }
        })
      }
    });
      
  }
  getBankCard(){
    reqwest_get('/api/users/bank_cards',{
      access_token:window.localStorage['token']
    },(data)=>{
      if(data.status=='success'){
        console.log(data)
        this.setState({bank_cards:data.list})
      }else{
        Toast.fail(data.msg,0.3)
      }
    });
  }
  handleClick(){
    let _this = this
  	Popup.show(<div  >
      <List   className={`${Reset.tel_list_header}`} renderHeader={() => (
        <div style={{ position: 'relative' ,fontSize:'0.28rem'}}>
          选择你的银行卡
          <span
            style={{
              position: 'absolute', right: 3, top: -5,
            }}
            onClick={() => this.onClose('cancel')}
          >
            <Icon type="cross" style={{width:'0.44rem',height:'0.44rem'}} />
          </span>
        </div>)}
        className="popup-list"
      >
        {_this.state.bank_cards.length>0?_this.state.bank_cards.map((i, index) => (
          <List.Item onClick={() => this.onClose(i)} className={`${Reset.tel_list}`} key={index}>{i.account}
            <List.Item.Brief>{i.code}</List.Item.Brief>
          </List.Item>
        )):<List.Item onClick={()=>{this.onClose(null);window.location.hash='/bank/new'}}  className={`${Reset.tel_list}`}>点击新增银行卡</List.Item>}
      </List>
    </div>, { animationType: 'slide-up', onMaskClose:() => Popup.hide()});
  }
  handleClickRecharge(){

  }
	render(){
    let _edit=null;
    let offsetX = -10; // just for pc demo
    if (/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)) {
      offsetX = -26;
    }
    const { getFieldProps,getFieldError } = this.props.form;

    const drawerProps = {
      open: this.state.open,
      position: this.state.position,
      onOpenChange: this.onOpenChange.bind(this),
    };

		return(
			<div style={{height:'100%'}}>
			  <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/pay'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>} iconName={false} onLeftClick={this.onOpenChange.bind(this)}>提现</NavBar>
				<WhiteSpace size='lg' />
 			<List >
 			  {this.state.bank_num==null?<ListItem onClick={this.handleClick.bind(this)} style={{height:'1.4rem'}} className={`${Reset.tel_list}`}>
         银行卡
        </ListItem>:<List.Item onClick={this.handleClick.bind(this)} style={{height:'1.4rem'}} className={`${Reset.tel_list}`} >{this.state.bank_num.account}
            <List.Item.Brief>{this.state.bank_num.code}</List.Item.Brief>
          </List.Item>}
 				
 				
 			</List>
 			<WhiteSpace size='xl' />
 			<List style={{background:'#fff'}}  className={`${Reset.tel_list_header} ${PayStyle.pay_list_nobor}`}>
      
 				<InputItem  placeholder='提现金额' type='money' {...getFieldProps('withdraw',{

              rules:[{required:true,message:'请输入你的提现金额'}]
            })}
            error={!!getFieldError('withdraw')}
            onErrorClick={() => {
              alert(getFieldError('withdraw').join('、'));
            }} className={`${Reset.tel_input} ${PayStyle.pay_input}`} >
 					￥
 				</InputItem>
 				
 			</List>
      <Button className={`${Reset.btn_moblie} ${Reset.tel_btn}`} type="primary" onClick={() => this.onWithdrawOk()}>提现</Button>
    </div>
		)
	}
}
// <div className={`${PayStyle.pay_list_fot}`}><span>可用余额</span><a>全部提现</a></div>
  // <InputItem  placeholder='支付宝账号' type='money' {...getFieldProps('alipay',{

        //       rules:[{required:true,message:'请输入你的支付宝账号'}]
        //     })}
        //     error={!!getFieldError('alipay')}
        //     onErrorClick={() => {
        //       alert(getFieldError('alipay').join('、'));
        //     }} className={`${Reset.tel_input} `} >
        //     支付宝账号
        // </InputItem>
  // <ListItem onClick={this.handleClick.bind(this)} style={{height:'1.4rem'}} className={`${Reset.tel_list}`}>
        //  银行卡
        // </ListItem>
const WithdrawalsForm = createForm()(Withdrawals);

export default connect()(WithdrawalsForm);
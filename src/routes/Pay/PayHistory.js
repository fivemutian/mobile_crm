import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace,ImagePicker} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import PayStyle from './Pay.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
import {reqwest_post,reqwest_get} from '../../utils/Reqwest.js';
const Item = List.Item;
const Brief =  List.Item.Brief;
class CustomerCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
      data:[],
			set:JSON.parse(window.localStorage['setting']),
			files:[],
			filesList:[],
      list:[],
      wallet:{amount:0,income:0,expend:0}
		}
	}
	onChange (files, type, index) {
    console.log(files, type, index);
    this.setState({
      files,
    });
  }
  getWallet(){
    let _this = this;
    reqwest_get('/api/wallets/me',{
        access_token:localStorage['token'],
      },(data) => {
        console.log(data);
        _this.setState({wallet:data.wallet,data:data.list});
        // console.log(this.state.wallet)
     })
  }
  handleImageClick(index,fs){
  	this.onChange.bind(this);
  }
	componentWillMount(){
    this.getWallet()
	}
	render(){
		console.log(this.state.list)
			const { set} =this.state;
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
     const ItemList = this.state.data.map(function(data){
      return <Link key={Math.random()*1} ><Item
        multipleLine
        className={`${Reset.tel_list}`}
        onClick={() => {}}
        platform="android"
      >
        时间：{data.time}<Brief>操作：{data.transfer}&ensp;<a>金额：{data.amount}</a></Brief>
      </Item></Link>
    })
		 return(
			<div className={`${style.layout}`}>
        <div className={`${PayStyle.pay}`}>
          <span>
            <span>我的金额</span><span>{_this.state.wallet.amount}</span>
          </span>
          <span>
            <span>总收入</span><span>{_this.state.wallet.income}</span>
          </span>
          <span>
            <span>总提成</span><span>{_this.state.wallet.expend}</span>
          </span>
        </div>
				<List style={{marginBottom:'0.3rem'}} className={`${Reset.tel_list_header}`} renderHeader={() => '历史'}>
          {this.state.data.length<1?<Item
              multipleLine
              onClick={() => {}}
              platform="android"
              className={`${style.dislike}`}
            >
              <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
              <Brief>暂无数据</Brief>
           </Item>:ItemList}
     	  </List>

        <WhiteSpace size='xl' />
        <WhiteSpace size="sm" />
        <div className={`${PayStyle.tab}`}>
          <Button component="div"><Link to={{pathname:'recharge'}}>充值</Link></Button>
          <Button type='primary'><Link className={`${style.white}`} to={{pathname:'withdra'}}>提现</Link></Button>
        </div>
			</div>
	 	)
		
	}
}
const CustomerForm = createForm()(CustomerCreate);
export default connect()(CustomerForm)
import React from 'react';
import { connect } from 'dva';
import { Picker,Radio, List, NavBar,Icon, InputItem , TabBar,DatePicker ,Toast ,Button,TextareaItem,WhiteSpace,Flex,Checkbox,Modal  } from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import NavStyle from '../../style/ResNav.less'
// import reqwest from 'reqwest';
import {bankvail} from '../../utils/Validator.js'
// import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
// import {range,disabledDate,disabledDateTime } from '../../utils/Date.js'
import moment from 'moment';
import {reqwest_get,reqwest_post} from '../../utils/Reqwest.js';
import {getBankBin} from 'bankcardinfo'
// import 'moment/locale/zh-cn';
class BankCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
      value:'saler_director',
      saler_boss:null,
      _regions:[],
      visible:false,
      bank_name:undefined,
      bank_card:undefined
		}
	}
	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    let _data = null;
    let _saler_id =null;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
        // console.log(JSON.parse(values.material[0]));
        console.log(values)
      if (!err  ) {
        reqwest_post('/api/users/add_bank_card',{
        	access_token:window.localStorage['token'],
        	bank_card:{code:values.bank_card,account:values.bank_name,branch:values.bank_branch,name:values.people_name}
        },(data)=>{
        	if(data.status=='success'){
        		Toast.success('更新成功',0.3,()=>{
        			window.location.hash='/bank/list';
        		});
        	}else{
        		Toast.fail(data.msg,0.3)
        	}
        });
      }
    });
	}
	componentWillMount(){
    let _this = this;
    let _regions = [];

	}
	handleChange(e){
    const {list} = this.state
    let _this = this;
    // let bankName;
    let _value=e.replace(/\s+/g,"")
    if(_value.length===19){
      getBankBin(_value).then(function (data) {
        console.log(data);
        // _list.account=data.bankName
        _this.setState({bank_name:data.bankName})
      }).catch(function (err) {
        console.log(err);
      })
    }
    _this.setState({
      bank_card : e
    })
  }
	render(){
		// console.log(this.state.list);
    const {value,value2,bank_card,bank_name} =this.state;
		const { getFieldProps,getFieldsError,getFieldsValue , getFieldError} = this.props.form;
		const _this =this;
    const _companies =[];
    const _material=[];
    let _ag=null;
    let naver=false;
    let saler_dir=null;
    // console.log(getFieldsError('phone'))
    // console.log(getFieldsError())
    // if(!getFieldsError()){
    //   naver = false;
    // }
    // console.log(this.state._re÷gions)
    // if(validateFields((err,values)))

		return(
			<div className={`${style.layout}`}>
			  <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/bank/list'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>} iconName={false} >添加银行卡</NavBar>
			  <WhiteSpace size='xl' />
				<List className={`${Reset.tel_list_header}`} >
          <InputItem
	          clear
            className={`${Reset.tel_input}`}
	          error={!!getFieldError('people_name')}
	          onErrorClick={() => {
	            alert(getFieldError('people_name').join('、'));
	          }}
            {...getFieldProps('people_name',{
            	rules:[{required:true,message:'请输入持卡人姓名'}]
            })}
            placeholder="持卡人"
          >持卡人</InputItem>
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('bank_card')}
            onErrorClick={() => {
              alert(getFieldError('bank_card').join('、'));
            }}
            {...getFieldProps('bank_card',{
            	initialValue:bank_card,
              rules:[{required:true,message:'银行卡号',validator:(rule,value,callback)=>{
            		!!value?bankvail(rule,value.replace(/\s+/g,""),callback):callback('1')
            	}}]
            })}
            onChange={(e)=>this.handleChange(e)}
            type='bankCard'
            placeholder="银行卡号"
          >银行卡号</InputItem> 
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('bank_name')}
            onErrorClick={() => {
              alert(getFieldError('bank_name').join('、'));
            }}
            {...getFieldProps('bank_name',{
            	initialValue:bank_name,
              rules:[{required:true,message:'银行名称'}]
            })}
            placeholder="银行名称"
          >银行名称</InputItem> 
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('bank_branch')}
            onErrorClick={() => {
              alert(getFieldError('bank_branch').join('、'));
            }}
            {...getFieldProps('bank_branch',{
              rules:[{required:true,message:'银行支行'}]
            })}
            placeholder="银行支行"
          >银行支行</InputItem>   
        </List>
        <WhiteSpace size="sm" />
        <WhiteSpace size="sm" />
        <Button disabled={naver}  className={`${Reset.btn_moblie} ${Reset.tel_btn}`} onClick={this.handleSubmit.bind(this)}  type='primary'>保存</Button>
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}

const BankNewForm = createForm()(BankCreate);
export default connect()(BankNewForm)
import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import { Drawer, List, NavBar ,Icon,Modal,WhiteSpace,InputItem,Popup,Button,Card,Toast} from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../../style/App.less'
import NavStyle from '../../style/ResNav.less'
import Reset from '../../style/ResetInput.less'
import PayStyle from './Pay.less'
// import reqwest from 'reqwest'
// import pingpp from 'pingpp-js'
import {reqwest_post,reqwest_get} from '../../utils/Reqwest.js';
const ListItem = List.Item;
class Bank extends React.Component{
	constructor(props){
		super(props);
		this.state={
			bank_cards:[]
		}
	}
	getBankCard(){
		reqwest_get('/api/users/bank_cards',{
			access_token:window.localStorage['token']
		},(data)=>{
			if(data.status=='success'){
				console.log(data)
				this.setState({bank_cards:data.list})
			}else{
				Toast.fail(data.msg,0.3)
			}
		});
	}
	componentWillMount(){
		this.getBankCard()
		window.sessionStorage.removeItem('bank')
	}
	render(){
		let _banks = this.state.bank_cards.map((data,index)=>{
			return (<Link  key={data.id} to={{pathname:'/bank/edit'}} onClick={()=>{window.sessionStorage['bank']=JSON.stringify(data)}}><div style={{padding:'0.2rem',background:'none'}}>
					<Card full className={`${Reset.tel_card}`}>
		      <Card.Header
		        title={data.account}
		      />
		      <Card.Body className={`${Reset.tel_card_body}`}>
		        <div>{data.code}</div>
		      </Card.Body>
		    </Card>
		   </div></Link>)
		})
		return(
			<div style={{height:'100%'}}>
			  <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} rightContent={<Link className={`${style.white}`} to={{pathname:'/bank/new'}}>新增</Link>} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/pay'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>} iconName={false} >银行卡</NavBar>
				<WhiteSpace size='lg' />
 					{_banks}
	 			<WhiteSpace size='xl' />
	 			<List style={{background:'#fff'}}  className={`${Reset.tel_list_header} ${PayStyle.pay_list_nobor}`}>
	      
	 			</List>
    </div>
		)
	}
}
export default connect()(Bank)
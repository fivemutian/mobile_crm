import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import { Drawer, List, NavBar ,Icon,Modal} from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../style/App.less'
import NavStyle from '../style/ResNav.less'
import Reset from '../style/ResetInput.less'
import reqwest from 'reqwest'
import MenuRi from '../components/Menu.js';
import New from '../components/Create.js';
import Active from '../components/Active.js'
import {New_case,New_edit} from '../utils/New.js';
import {svg_url,} from '../utils/Svg.js';
class App extends React.Component{
	constructor(props){
		super(props);
		this.state={
			dacked:false,
			open: false,
      position: 'left',
      menu:[JSON.parse(window.localStorage['menu'])],
      visible:false,
      prevPathName:'',
      set:JSON.parse(window.localStorage['setting']),
      edit:'null',
      url:'/home'
		}
	}
	onDock(d){
		this.setState({
      [d]: !this.state[d],
    });
	}
	 onOpenChange = (...args) => {
    console.log(args);
    this.setState({ open: !this.state.open });
    this.setState({url:window.sessionStorage['new']})
  }
  componentWillMount(){
    // console.log(navigator.platform)
    console.log(!!this.props.location.pathname.match(/inforder/g))
    let _menu;
    console.log(navigator.platform)
    if(this.state.set.role==='acct'){
      _menu=[{url:'pay',title:'钱包'},{url:"acctor",title:'订单'}];
    }else{
      _menu=[{title:'主页',url:'home'}];
      _menu.push({url:'pay',title:'钱包'})
      for(let i in this.state.menu[0]){
        // console.log(_menu)
          _menu.push({url:i,title:this.state.menu[0][i]})
      }
    }
    this.setState({menu:_menu,})
    if(New_edit[this.props.location.pathname]!=undefined){
      this.setState({edit:1})
    }else if(!!this.props.location.pathname.match(/edit/g)){
      this.setState({edit:'edit'})
    }else if(New_case[this.props.location.pathname]!=undefined){
      this.setState({edit:'create'})
    }else{
      this.setState({edit:'2'})
    }
    console.log(1)
  }
  componentsDidMount(){
    
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps)
    if(New_edit['/'+nextProps.routes[0].path+'/'+nextProps.routes[1].path]!=undefined){
      this.setState({edit:1})
    }else if(!!nextProps.routes[1].path.match(/edit/g)){
      this.setState({edit:'edit'})
    }else if(New_case['/'+nextProps.routes[0].path+'/'+nextProps.routes[1].path]!=undefined){
      this.setState({edit:'create'})
    }else{
      this.setState({edit:2})
    }

  }
	render(){
    let _edit=null;
    let offsetX = -10; // just for pc demo
    if (/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)) {
      offsetX = -26;
    }
    if(this.state.edit==1){
      _edit = <MenuRi location={this.props.location.pathname} ></MenuRi>
    }else if(this.state.edit=='create'){
      console.log(New_case[this.props.location.pathname])
      // _edit = <Active url={this.props.location.pathname}></Active>
      _edit =<Link style={{fontSize:'0.32rem',color:'#fff'}} to={{pathname:New_case[this.props.location.pathname].url}}>{New_case[this.props.location.pathname].text}</Link>
    }else{
      _edit='';
    }
    const _avatar=(this.state.set.avatar_url==null)?"https://images.chuanggj.com/uploads/user/headimg/80/1490154626_%E5%BB%BA%E7%AD%913.png":this.state.set.avatar_url;
    
		const sidebar = (<List onClick={(e)=>{this.setState({open:false})}} className={`${style.list} ${NavStyle.tel_sider}`}>
      <List.Item multipleLine> 
        <div className={`${style.list_avatar}`}>
          <img src={_avatar} />
          <p>{this.state.set.name}</p>
        </div>
      </List.Item>
      {this.state.menu.map((i, index,a) => {

        if((a.length)!==index&&i.title!='品牌'&&i.title!='品牌审核'){
          return (<Link onClick={()=>{console.log(window.sessionStorage['new']);}}  key={index} to={{pathname:'/hfive/'+i.url}}><List.Item
            thumb={<img src={svg_url[i.url]} />}
            multipleLine
          >{i.title}</List.Item></Link>);
        }else if(JSON.parse(window.localStorage['setting']).role=='admin'){
          return (<Link   key={index} to={{pathname:'/hfive/'+window.localStorage['account_type']}}><List.Item 
           thumb={<img src={svg_url['pinpai']} />}
          >{i.title}</List.Item></Link>);
        }
      })}
      <Link to={{pathname:'/hfive/set'}}> <List.Item thumb={<img src={svg_url['setting']} />}
            multipleLine>设置
      </List.Item></Link>
      <List.Item onClick={() => Modal.alert(<span className={`${Reset.tel_modal_title}`}>退出</span>, <span className={`${Reset.tel_modal_content}`}>确定退出么???</span>, [
      { text: <span className={`${Reset.tel_modal_btn}`}>取消</span>, onPress: () => console.log('cancel') },
      { text: <span className={`${Reset.tel_modal_btn}`}>确定</span>, onPress: () => {window.localStorage.clear();window.location.hash='/log';}, style: { fontWeight: 'bold' } },
    ])}
          thumb={<img src={svg_url['exit']} />}
        >退出</List.Item>
    </List>);
    const drawerProps = {
      open: this.state.open,
      position: this.state.position,
      onOpenChange: this.onOpenChange.bind(this),
    };

		return(
			<div style={{height:'100%'}}>
			<NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} rightContent={_edit} iconName={false} leftContent={<Icon style={{height:'0.44rem',width:'0.44rem'}} type={require('!svg-sprite!../assets/Path/menubar.svg')} />} onLeftClick={this.onOpenChange.bind(this)}>管家快销</NavBar>
      <Drawer
        className={`${style.my_drawer} `}
        style={{ minHeight: document.documentElement.clientHeight - 100 }}
        dragHandleStyle={{ display: 'none' }}
        contentStyle={{ color: '#A6A6A6', textAlign: 'left',height:'100%'}}
        sidebarStyle={{height:'100%',width:'65vw',zIndex:1000}}
        sidebar={sidebar}
        {...drawerProps}
      >
       {this.props.children}
      </Drawer>
    </div>
		)
	}
}
export default connect()(App);
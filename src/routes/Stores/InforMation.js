import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Card,TarBar} from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
class StoresLink extends React.Component{
  constructor(props){
    super(props);
    this.state={
      list:[],
      pageCurrent:0,
      animating:false,
      ism:JSON.parse(window.localStorage["ism"])
    }
  }
  componentWillMount(){
    // alert(localStorage['token'])
    // if(window.location.host=="172.16.0.249:8181"){
    //  // sessionStorage['token']="9aySPxzAmrO2xwg/PKFFKyOR/Tz3AB7OsmxH2kRUn/S2X3HFwtv386zl3gRB8nOkUdo4zUsqepccrCe1BgPrTA=="
    // }
    // if(JSON.parse(window.sessionStorage["setting"]).role=='admin'){
    	window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editsroes","text":"编辑"}]);
    	this.setState({aaa:'aa'})
    // }
  }
  handelPageChage(e,tar){
    // this.setState({pageCurrent:e})
    // this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillUnmount(){
    console.log(1);
    // window.sessionStorage.removeItem('bianji'); 
  }
  render(){
    const {ism} = this.state;
    return(
      <div className="flex-container">
        <List renderHeader={() => '渠道信息'}  className={`${style.infor}`}>
          <Brief><span>渠道名称:</span><span>{ism.name}</span></Brief>
          <Brief><span>门店名称:</span><span>{ism.region_name}</span></Brief>
          <Brief><span>联系人:</span><span>{ism.contact}</span></Brief>
          <Brief><span>手机号码:</span><span>{ism.phone}</span></Brief>
          <Brief><span>地址:</span><span>{ism.address}</span></Brief>
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <List renderHeader={() => '基础信息'}  className={`${style.infor}`}>
          <Brief><span>创建时间:</span><span>{ism.created_at.match(/\d+\-\d+\-\d+/)[0]}</span></Brief>
          <Brief><span>修改时间:</span><span>{ism.updated_at.match(/\d+\-\d+\-\d+/)[0]}</span></Brief>
        </List>

      </div>
    )
  }
}
export default connect()(StoresLink)
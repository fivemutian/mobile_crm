import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
class Stores extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
			stores:[],
      pageCurrent:0,
      animating:false,
      pre:5
		}
	}
	loadServer(params={} ) {
    // alert(params);
    let _data=[];
    let _this=this;
    this.setState({ animating: true });
    reqwest({
      url: window.host+'/api/stores',
      method: 'get',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        page:params,
        per: _this.state.pre
      },
      type: 'json',
    }).then((data) => {
      // alert(data.status);
      // sessionStorage['companies']=JSON.stringify(data.companies);
      // sessionStorage['material']=JSON.stringify(data.materials);
      // sessionStorage['regions']=JSON.stringify(data.regions);
      const pagination = { ...this.state.pagination };
      this.setState({
        animating: false,
        // data: _data,
        stores:data.regions,
        list:data.list,
        pagination:data.total,
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
  }
  componentWillMount(){
  	this.loadServer(1)
    if(JSON.parse(window.localStorage["setting"]).role=='admin'){
       window.sessionStorage['new']='/hfive/newsroes';

      window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editsroes","text":"编辑"}]);
      this.setState({aaa:'aa'})
    }
  }
  componentWillUnmount(){
    
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
	render(){
		const {stores} = this.state;
		const ItemList = this.state.list.map(function(data){
			return <Link key={data.id} onClick={()=>{window.localStorage["ism"]=JSON.stringify(data)}} to={{pathname:'/hfive/infosroes'}}><Item
        arrow="horizontal"
        multipleLine
        className={`${Reset.tel_list}`}
        onClick={() => {}}
        platform="android"
      >
        渠道名称:{data.name}<Brief>门店名称:{data.name+data.address}</Brief>
        <Brief>联系人:{data.contact}</Brief><Brief>手机号码:{data.phone}</Brief>
      </Item></Link>
		})
		return(
			<div className="flex-container">
        <List renderHeader={() => '渠道/门店'} className={`${Reset.tel_list_header}`}>
      	  {ItemList}
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/this.state.pre)} 
		      locale={{
		        prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
		        nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
		      }} />
			</div>
		)
	}
}
export default connect()(Stores)
import React from 'react';
import { connect } from 'dva';
import { List, WhiteSpace ,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../../style/App.less'
import Reset from '../../../style/ResetInput.less'
import SalerDirector from './../SalerDirector.js'
// import reqwest from 'reqwest';
import PropTypes from 'prop-types'
import Details from './Details.js';
import Save from './Save.js';
import Acct from './Acct.less'
import {reqwest_get,reqwest_url} from '../../../utils/Reqwest.js'
const Item = List.Item;
const Brief = Item.Brief;
class Order extends React.Component{
	constructor(props){
		super(props);
		this.state={
      data:[],
      list:[],
      save:[],
      order:[],
      typ:'save'
		}
	}
  handleSubmit(e,_from){
    e.preventDefault();
    let _this =this;

    _from.validateFields((err, values) => {
      console.log(values);
      // return
      if (!err) {
        // reqwest_post('/api/strategy_results/'+this.props.url.query.uu,{
        //   access_token:window.localStorage['token'],
        //   strategy_result:values
        // },(data)=>{
        //   // console.log(2)
        //   // _this.context.router.goBack()
        //   _this.context.router.push('/hfive/listacct?uu='+this.props.url.query.uu+'&typ=list')
        // })
        _this.loadServer({data:{
          access_token:window.localStorage['token'],
          strategy_result:values
        },method:'post'})
         _this.context.router.push('/hfive/listacct?uu='+_this.props.location.query.uu+'&typ=list')
      }
    });
  }
	loadServer(params={} ) {
    // alert(params);
    let _data , _save;
    // reqwest_get('/api/strategy_results/1',{
    reqwest_url('/api/strategy_results/'+this.props.location.query.uu,params.data,params.method,(data)=>{
      if(data.status==='success'){
        _data=[{text:'订单号',value:data.sr.order.serial_number},{text:'面积',value:data.sr.order.square},
        {text:"预约测量时间",value:data.sr.order.booking_date},{text:'订单所有者',value:data.sr.order.owner_name},
        {text:'安装费/测量费（￥）',value:data.sr.order.owner_name},
        {text:"产品价格",value:data.sr.order.estimated_total},{text:'渠道',value:data.sr.order.region_name},
        {text:'门店',value:data.sr.order.store_name}]
        _save = [{text:'销售提成',id:'saler_rate_amount',value:data.sr.saler_rate_amount},
        {text:'销售提成',id:'saler_director_rate_amount',value:data.sr.saler_director_rate_amount},
        {text:'介绍人返利',id:'saler_rate_amount',value:data.sr.introducer_rebate_amount},
        {text:'运营提成',id:'operator_amount',value:data.sr.operator_amount},
        {text:'售后提成',id:'cs_director_amount',value:data.sr.cs_director_amount},
        {text:'企划提成',id:'planinger_director_amount',value:data.sr.planinger_director_amount}]
        this.setState({
          data:_data,
          save:_save,
          list:data.sr,
          order:data.sr.order
        })
      }
    })
    this.setState({animating:false})
  }
  componentWillMount(){
  	this.loadServer({data:{
      access_token:window.localStorage['token'],
    },method:'get'})
    // window.sessionStorage['new']='/hfive/newor';
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editor","text":"编辑"}]);
    
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillUnmount(){
  }
	render(){
    // console.log(this.state.data)
    const {data,save} = this.state;
    const {typ} = this.props.location.query;
    const _node = {
      "list":<Details data={save} url={this.props.location} />,
      "save":<Save data={save} url={this.props.location} callback={this.handleSubmit.bind(this)}/>
    }
    // const _dataNode = this.state.data.map((data,index)=>{
    //  return <Item extra={'extra content'} >{data.value}</Item>
    // })
    // if(data.length>0){

    const _dataNode = this.state.data.map((data,index)=>{
     return <Item  className={`${Reset.tel_list} ${Acct.acct_list}`} style={{padding:'0rem 0rem 0rem 0.25rem',borderBottom:'none!important'}} key={index} >{data.text+'：'+data.value}</Item>
    })
    // }
		return(
			<List className="flex-container">
        {_dataNode}
        {_node[typ]}
			</List>
		)
	}
}
// const OrderForm = createForm()(Order);
Order.contextTypes = {
  router: PropTypes.object
};
export default connect()(Order)
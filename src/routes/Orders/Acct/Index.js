import React from 'react';
import { connect } from 'dva';
import {  List, NavBar,Icon, WhiteSpace, TabBar,Pagination,ActivityIndicator,Tabs } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../../style/App.less'
import Reset from '../../../style/ResetInput.less'
// import reqwest from 'reqwest';
import {reqwest_get} from '../../../utils/Reqwest.js'
const Item = List.Item;
const Brief = Item.Brief;
const TabPane = Tabs.TabPane;
class Acct extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false,

		}
	}
	fetch(params={}){
		this.setState({animating:true})
		reqwest_get("/api/strategy_results",{
			access_token:window.localStorage['token'],
			state:params.tabs,
			page:params.current
		},(response)=>{
			const pagination = { ...this.state.pagination };
      pagination.total = response.total;
			console.log(response.list)
			this.setState({
        animating: false,
        // response: _data,
        list:response.list,
        pagination:pagination.total,
      });
		})
	}
	handelTabsChage(e){
    this.state.tab=e;
    // this.state.pageCurrent=1
    console.log(e)
    this.setState({pageCurrent:0})
    console.log(this.state.tab)
    this.fetch({current:1,tabs:e})
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.fetch({current:e,tabs:this.state.tabs});
    // console.log(tar)
  }
	componentWillMount(){
		this.fetch({tabs:false})
	}
	render(){
    let _this = this;
		const ItemList = this.state.list.map(function(data){
			return <Link key={data.id} to={{pathname:'/hfive/listacct',query:{tab:_this.state.tab,uu:data.id,typ:'list'}}} onClick={()=>{window.sessionStorage["ism"]=JSON.stringify(data)}}><Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        platform="android"
        className={`${Reset.tel_list}`}
      >
        订单号:{data.serial_number}<Brief>客户名称:{data.customer_name}</Brief>
        <Brief>订单所有者:{data.saler_name}</Brief>
        <Brief>手机:{data.saler_mobile}</Brief>
        <Brief>提成:{data.saler_rate_amount}</Brief>
      </Item></Link>
		})
		return(
			<Tabs className={`${Reset.reset_tabs}`} defaultActiveKey="false" onChange={this.state.handelTabsChage} onTabClick={(e)=>{this.handelTabsChage(e)}}>
        <TabPane  tab="未支付订单" key="false">
          <List   className={`${Reset.tel_list_header}`}>
            {this.state.list.length<1?<Item
                className={`${Reset.tel_list}`}
                arrow="horizontal"
                multipleLine
                onClick={() => {}}
                platform="android"
                className={`${style.dislike}`}
              >
                <Icon type={require('!svg-sprite!../../../assets/Path/dislike.svg')} />
                <Brief>暂无数据</Brief>
             </Item>:ItemList}
            <ActivityIndicator
              toast
              text="正在加载"
              animating={this.state.animating}
            />
          </List>
        <Pagination onChange={this.state.handelPageChage} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/10)} 
          locale={{
            prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
            nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
          }} />
        </TabPane>
        <TabPane  tab="以支付订单" key="true">
          <List   className={`${Reset.tel_list_header}`}>
            {this.state.list.length<1?<Item
                className={`${Reset.tel_list}`}
                arrow="horizontal"
                multipleLine
                onClick={() => {}}
                platform="android"
                className={`${style.dislike}`}
              >
                <Icon type={require('!svg-sprite!../../../assets/Path/dislike.svg')} />
                <Brief>暂无数据</Brief>
             </Item>:ItemList}
            <ActivityIndicator
              toast
              text="正在加载"
              animating={this.state.animating}
            />
          </List>
          <Pagination onChange={this.state.handelPageChage} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/10)} 
            locale={{
              prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
              nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
            }} />
        </TabPane>
      </Tabs>

		)
	}
}
export default Acct
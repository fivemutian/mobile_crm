import React from 'react';
import { connect ,bindActionCreators} from 'dva';
import {  List,WhiteSpace,InputItem} from 'antd-mobile';
import { Link } from 'dva/router';
import style from '../../../style/App.less'
import Reset from '../../../style/ResetInput.less'
import Acct from './Acct.less'
import PropTypes from 'prop-types'
import { createForm } from 'rc-form'
import {reqwest_post} from '../../../utils/Reqwest.js'
const Item = List.Item;
const Brief = Item.Brief;

class Save extends React.Component{
	// handleSubmit(e){
	// 	e.preventDefault();
 //    let _this =this;
 //    let { state, dispatch } = this.props
 //    actions={editAcct: actions.editAcct }
 //    console.log(actions)
 //    this.props.form.validateFields((err, values) => {
 //      console.log(values);
 //      // return
 //      if (!err) {
 //        reqwest_post('/api/strategy_results/'+this.props.url.query.uu,{
	// 				access_token:window.localStorage['token'],
	// 				strategy_result:values
	// 			},(data)=>{
	// 				// this.props.callback(data)
	// 				// console.log(2)
	// 				// _this.context.router.goBack()
	// 				_this.context.router.push('/hfive/listacct?uu='+this.props.url.query.uu+'&typ=list')
	// 			})
 //      }
 //    });
	// }
	render(){
		let { state, dispatch } = this.props;
		// let mockActions = {
		//     eatApple : id => console.log('eatApple',id), //指定了函数的签名
		//     foo: (arg1,arg2) => console.log('foo',arg1,arg2) //也响应了调用测试
		// };

		// state = mockState; actions = mockActions; 
		// console.log(this.state)
		const { getFieldProps,getFieldError } = this.props.form;
		const _dataNode = this.props.data.map((data,index)=>{
     return <InputItem  className={`${Reset.tel_input} ${Acct.acct_input}`}  key={index} error={!!getFieldError(data.id)}
            onErrorClick={() => {
              alert(getFieldError(data.id).join('、'));
            }}
            {...getFieldProps(data.id,{
            	initialValue:data.value
            })} >{data.text}</InputItem>
    })
		return(
			<div className={`${Acct.last}`}>
        {_dataNode}
        <WhiteSpace size='xl'  style={{background:'#efefef'}}/>
        <div className={`${style.tab}`}  style={{marginTop:'1rem'}}>
          <Link onClick={(e)=>this.props.callback(e,this.props.form)}>修改</Link>
          <Link style={{background:'#108ee9',color:'#fff'}} to={{pathname:'/hfive/acctor',query:{typ:'list'}}}>返回</Link>
        </div>
			</div>
		)
	}
}
const SaveForm = createForm()(Save);

Save.contextTypes = {
  router: PropTypes.object
};
// function buildActionDispatcher(dispatch) {
//   return {
//       actions: bindActionCreators(actions, dispatch)
//     }
// }
function select(state) {
	console.log(state)
    return {
        state: state.appleBusket
    }
}
export default connect(select)(SaveForm)
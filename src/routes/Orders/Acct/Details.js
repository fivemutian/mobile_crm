import React from 'react';
import { connect } from 'dva';
import {  List,WhiteSpace} from 'antd-mobile';
import { Link } from 'dva/router';
import style from '../../../style/App.less'
import Reset from '../../../style/ResetInput.less'
import Acct from './Acct.less'
const Item = List.Item;
const Brief = Item.Brief;

class Details extends React.Component{
	render(){
		const {url} = this.props
		const _dataNode = this.props.data.map((data,index)=>{
     return <Item  className={`${Reset.tel_list} ${Acct.acct_list}`}  key={index} >{data.text+'：'+data.value}</Item>
    })
		return(
			<div className={`${Acct.last}`}>
        {_dataNode}
        <WhiteSpace size='xl'  style={{background:'#efefef'}}/>
        <div className={`${style.tab}`}  style={{marginTop:'1rem'}}>
          <Link to={{pathname:'/hfive/listacct',query:{typ:'save',uu:url.query.uu}}}>修改</Link>
          <Link style={{background:'#108ee9',color:'#fff'}} onClick={()=>this.showModalUpdate('更改介绍人返利','rebate')}>支付</Link>
        </div>
			</div>
		)
	}
}
export default connect()(Details)
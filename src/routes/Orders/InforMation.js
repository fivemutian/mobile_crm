import React from 'react';
import { connect } from 'dva';
import {  List,Icon, WhiteSpace ,ActivityIndicator,Modal,Toast} from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import {reqwest_post} from '../../utils/Reqwest.js'
const Item = List.Item;
const Brief = Item.Brief;
// let _update={
//   'admin':<div className={`${style.tab}`}>
//           <Link onClick={()=>this.showModalUpdate()}>更改产品预估价格</Link>
//           <Link style={{background:'#108ee9',color:'#fff'}} onClick={()=>this.showModalUpdate()}>更改介绍人返利</Link>
//         </div>,
//   "qita":<div className={`${style.tab}`}>
//           <Link onClick={()=>this.showModalUpdate()}>更改产品预估价格</Link>
//         </div>
// }
class OrderInfo extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false,
      ism:JSON.parse(window.sessionStorage["ism"]),
      visible:false,
      _update:{
        'admin':<div className={`${style.tab}`}>
          <Link onClick={()=>this.showModalUpdate('更改产品预估价格','estimated_total')}>更改产品预估价格</Link>
          <Link style={{background:'#108ee9',color:'#fff'}} onClick={()=>this.showModalUpdate('更改介绍人返利','rebate')}>更改介绍人返利</Link>
        </div>,
        "qita":<div className={`${style.tab}`}>
          <Link onClick={()=>this.showModalUpdate('更改产品预估价格','estimated_total')}>更改产品预估价格</Link>
        </div>
      }
		}
	}
  componentWillMount(){
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editor","text":"编辑"},{"url":"del","text":"删除"}]);
  }
  showModalUpdate(title,_text){
    let _this =this;
    Modal.prompt(<span className={`${Reset.tel_modal_title}`}>{title}</span>,<span className={`${Reset.modal_res_input}`}></span> , [
      { text: <span className={`${Reset.tel_modal_btn}`}>取消</span>, onPress: () => console.log('cancel') },
      { text: <span className={`${Reset.tel_modal_btn}`}>确定</span>, onPress: (e) => {this.handleUpdate(e,_this,_text)}, style: { fontWeight: 'bold' } },
    ],'plain-text',undefined)
    // this.setState({visible:true})
  }
  handleUpdate(e,_this,_text){
    console.log(_text)
    // let _this = this;
    let _ism = this.state.ism;
    let _data={
      'estimated_total':{
        access_token:window.localStorage['token'],
        id:this.state.ism.id,
        estimated_total:e
      },
      rebate:{
        access_token:window.localStorage['token'],
        id:this.state.ism.id,
        rebate:e/100
      }
    }
    let _boolean = {
      'estimated_total':{url:'/api/orders/update_estimated_total'},
      "rebate":{url:'/api/orders/update_rebate'}
    }
    reqwest_post(_boolean[_text].url,_data[_text],(data)=>{
      if(data.status==='success'){
        // message.success('更新成')
        Toast.success('更新成功',0.3)
        // _boolean[_text].total;
        _text==='rebate'?_ism.rebate=e/100:_ism.estimated_total=e;
        // console.log(_boolean[_text].total)
        console.log(_ism)
        _this.setState({ism:_ism});
        window.sessionStorage['ism']=JSON.stringify(this.state.ism)
      }else{
        // message(data.msg,0.4)
        Toast.fail(data.msg,0.3)
      }
    })
  }
	render(){
    // console.log(this.state.ism)

    const {ism} = this.state;
    let _straty = ism.strategy_result.map((data)=>{

      if(data[0]=='服务费'){
        return <Brief key={data[1]}><span>{data[0]}:</span><span>{data[1]}</span></Brief>
      }else if(data[0]==='介绍人返利'){
        return <Brief key={data[1]}><span>{data[0]}:</span><span>{ism.rebate*100+'%'}</span></Brief>
      }else if(data[0]==='产品预估价格'){
        return <Brief key={data[1]}><span>{data[0]}:</span><span>{ism.estimated_total}</span></Brief>
      }
    })
		// const ItemList = this.state.list.map(function(data){
		// 	return <Item
  //       arrow="horizontal"
  //       multipleLine
  //       onClick={() => {}}
  //       key={data.id}
  //       platform="android"
  //     >
  //       姓名:{data.name}<Brief>电话:{data.mobile}</Brief>
  //     </Item>
		// })
		return(
			<div className="flex-container">
        <List renderHeader={() => '订单信息'} style={{marginBottom:'2rem'}}  className={`${style.infor}`}>
          <Brief><span>订单号:</span><span>{ism.serial_number}</span></Brief>
      	  <Brief><span>姓名:</span><span>{ism.name}</span></Brief>
          <Brief><span>电话:</span><span>{ism.tel}</span></Brief>
          <Brief><span>材质:</span><span>{ism.material}</span></Brief>
          <Brief><span>预估面积:</span><span>{ism.square}</span></Brief>
          <Brief><span>测量师傅:</span><span>{ism.measure_master}</span></Brief>
          <Brief><span>渠道:</span><span>{ism.region_name}</span></Brief>
          <Brief><span>门店:</span><span>{ism.store_name}</span></Brief>
          <Brief><span>地址:</span><span>{ism.province+ism.city+ism.area+ism.street}</span></Brief>
          {_straty}
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        {this.props.location.query.tab=='2'?undefined:this.state._update[JSON.parse(window.localStorage['setting']).role]}
			</div>
		)
	}
}
export default connect()(OrderInfo)
import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Tabs } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
const TabPane = Tabs.TabPane;
class SalerDorectoe extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false
		}
	}
	render(){
    let _this = this;
		const ItemList = this.props.list.map(function(data){
			return <Link key={data.id} to={{pathname:'/hfive/inforder',query:{tab:_this.props.tab}}} onClick={()=>{window.sessionStorage["ism"]=JSON.stringify(data)}}><Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        platform="android"
        className={`${Reset.tel_list}`}
      >
        订单号:{data.serial_number}<Brief>姓名:{data.name}</Brief>
        <Brief>电话:{data.tel}</Brief>
        <Brief>状态:{data.workflow_state}</Brief>
        <Brief>预估产品价格:{data.estimated_total}</Brief>
      </Item></Link>
		})
		return(
			<Tabs className={`${Reset.reset_tabs}`} defaultActiveKey="1" onChange={this.props.handelTabsChage} onTabClick={(e)=>{console.log(e)}}>
        <TabPane  tab="我的订单" key="1">
          <List   className={`${Reset.tel_list_header}`}>
            {this.props.list.length<1?<Item
                className={`${Reset.tel_list}`}
                arrow="horizontal"
                multipleLine
                onClick={() => {}}
                platform="android"
                className={`${style.dislike}`}
              >
                <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
                <Brief>暂无数据</Brief>
             </Item>:ItemList}
            <ActivityIndicator
              toast
              text="正在加载"
              animating={this.props.animating}
            />
          </List>
        <Pagination onChange={this.props.handelPageChage} current={this.props.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.props.pagination/10)} 
          locale={{
            prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
            nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
          }} />
        </TabPane>
        <TabPane  tab="团队订单" key="2">
          <List   className={`${Reset.tel_list_header}`}>
            {this.props.list.length<1?<Item
                className={`${Reset.tel_list}`}
                arrow="horizontal"
                multipleLine
                onClick={() => {}}
                platform="android"
                className={`${style.dislike}`}
              >
                <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
                <Brief>暂无数据</Brief>
             </Item>:ItemList}
            <ActivityIndicator
              toast
              text="正在加载"
              animating={this.props.animating}
            />
          </List>
          <Pagination onChange={this.props.handelPageChage} current={this.props.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.props.pagination/10)} 
            locale={{
              prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
              nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
            }} />
        </TabPane>
      </Tabs>

		)
	}
}
export default SalerDorectoe
import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import SalerDirector from './SalerDirector.js'
import Acct from './Acct/Index.js'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;

class Order extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      role:JSON.parse(window.localStorage['setting']).role,
      animating:false,
      tab:"1"
		}
	}
	loadServer(params={} ) {
    // alert(params);
    let _data=[];
    let _tabs={
      "1":{
        access_token:localStorage['token'],
        page:params,
        pre:10
      },
      "2":{
        access_token:localStorage['token'],
        page:params,
        pre:10,
        type:"children"
      }
    }
    let _this=this;
    this.setState({ animating: true });
    reqwest({
      url: window.host+'/api/orders',
      method: 'get',
      crossOrigin: true,
      data: _tabs[this.state.tab],
      type: 'json',
    }).then((data) => {
      // alert(data.status);
      // sessionStorage['companies']=JSON.stringify(data.companies);
      // sessionStorage['material']=JSON.stringify(data.materials);
      // sessionStorage['store_tree']=JSON.stringify(data.stores_tree);
      const pagination = { ...this.state.pagination };
      pagination.total = data.total;
      this.setState({
        animating: false,
        // data: _data,
        list:data.list,
        pagination:pagination.total,
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
    this.setState({animating:false})
  }
  componentWillMount(){
  	this.loadServer(1)
    window.sessionStorage['new']='/hfive/newor';
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editor","text":"编辑"}]);
    
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
  handelTabsChage(e){
    this.state.tab=e;
    // this.state.pageCurrent=1
    this.setState({pageCurrent:0})
    console.log(this.state.tab)
    this.loadServer(1)
  }
  componentWillUnmount(){
  }
	render(){
    let _list;
		const ItemList = this.state.list.map(function(data){
			return <Link key={data.id} to={{pathname:'/hfive/inforder'}} onClick={()=>{window.sessionStorage["ism"]=JSON.stringify(data)}}><Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        platform="android"
        className={`${Reset.tel_list}`}
      >
        订单号:{data.serial_number}<Brief>姓名:{data.name}</Brief>
        <Brief>电话:{data.tel}</Brief>
        <Brief>状态:{data.workflow_state}</Brief>
        <Brief>预估产品价格:{data.estimated_total}</Brief>
      </Item></Link>
		})
    if(this.state.role=='saler_director'){
      _list=<SalerDirector tab={this.state.tab} list={this.state.list} handelTabsChage={(e)=>{this.handelTabsChage(e)}}  handelPageChage={(e)=>this.handelPageChage(e)} animating={this.state.animating} pagination={this.state.pagination} />
    }else{
      _list=<div><List renderHeader={() => '订单详情'}  className={`${Reset.tel_list_header}`}>
        {this.state.list.length<1?<Item
          multipleLine
          onClick={() => {}}
          platform="android"
          className={`${style.dislike}`}
        >
          <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
          <Brief>暂无数据</Brief>
        </Item>:ItemList}
        <ActivityIndicator
          toast
          text="正在加载"
          animating={this.state.animating}
        />
        </List>
        <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/10)} 
        locale={{
          prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
          nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
        }} />
      </div>
    }
		return(
			<div className="flex-container">
        
      	  {_list}
         
        
        
			</div>
		)
	}
}
export default connect()(Order)
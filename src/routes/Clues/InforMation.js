import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Card,TarBar} from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
// import ClueStyle from './Clue.less'
const Item = List.Item;
const Brief = Item.Brief;
class Clues extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false,
      ism:JSON.parse(window.sessionStorage["ism"])
		}
	}
  componentWillMount(){
    // alert(localStorage['token'])
  	// if(window.location.host=="172.16.0.249:8181"){
  	// 	// sessionStorage['token']="9aySPxzAmrO2xwg/PKFFKyOR/Tz3AB7OsmxH2kRUn/S2X3HFwtv386zl3gRB8nOkUdo4zUsqepccrCe1BgPrTA=="
  	// }
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editcl","text":"编辑"},{"url":"del","text":"删除","method":"/api/clues/"}]);
    console.log(this.state.ism.remark.split('~'))
    this.setState({
      edit:1
    })
  }
  handelPageChage(e,tar){
    // this.setState({pageCurrent:e})
    // this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillUnmount(){
    console.log(1);
    // window.sessionStorage.removeItem('bianji'); 
  }
	render(){
    const {ism} = this.state;
    let _remark;
		const ItemList = this.state.list.map(function(data){
			return <Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        key={data.id}
        platform="android"
      >
        姓名:{data.name}<Brief>电话:{data.mobile}</Brief>
      </Item>
		})
    _remark=ism.remark.split('~')[0]===''||ism.remark.split('~')[0]===null?'空':ism.remark.split('~');
		return(
			<div className="flex-container">
        <List renderHeader={() => '线索信息'}  className={`${style.infor}`}>
      	  <Brief><span>姓名:</span><span>{ism.name}</span></Brief>
          <Brief><span>电话:</span><span>{ism.mobile}</span></Brief>
          <Brief><span>创建者:</span><span>{ism.name}</span></Brief>
          <Brief><span>地址:</span><span>{ism.address}</span></Brief>
          <Brief><span>备注:</span><span>{_remark==='空'?'空':_remark.map((data,index,obj)=>{
            return <p  key={index}>
              {JSON.parse(data).blocks.map((dats)=>{
                console.log(dats)
                return <strong key={dats.key}  key={dats.key}><strong>{dats.text}</strong></strong>
              })}
              <span style={{textAlign:'right',display:'block',marginRight:'0.1rem'}}>{JSON.parse(data).date}</span>
            </p>
          })}</span></Brief>
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <List renderHeader={() => '基础信息'}  className={`${style.infor}`}>
          <Brief><span>创建时间:</span><span>{ism.created_at.match(/\d+\-\d+\-\d+/)[0]}</span></Brief>
          <Brief><span>修改时间:</span><span>{ism.updated_at.match(/\d+\-\d+\-\d+/)[0]}</span></Brief>
        </List>
        <div className={`${style.tab}`}>
          <Link to={{pathname:'/hfive/newor',query:{name:ism.name,mobile:ism.mobile}}}>下单</Link>
        </div>
			</div>
		)
	}
}
// <TabBar>
  // <TabBar.Item
    // title="返回"
    // key="返回"
    // icon={<Icon type='left' />
    // }
    // selectedIcon={<Icon type='left' style={{color:'#108ee9'}} />}
    // selected={this.state.selectedTab === 'blueTab'}
    // onPress={() => {
      // this.setState({
        // selectedTab: 'blueTab',
      // });
      // window.location.hash='/hfive/clues';
    // }}
    // data-seed="logId"
  // >
  // <Link to={{pathname:'/hfive/clues'}}></Link>
  // </TabBar.Item>
  // <TabBar.Item
    // title="编辑"
    // key="编辑"
    // icon={<Icon type={require('!svg-sprite!../../assets/Path/setting.svg')} />
    // }
    // selectedIcon={<Icon type={require('!svg-sprite!../../assets/Path/setting.svg')} style={{color:'#108ee9'}} />}
    // selected={this.state.selectedTab === 'editTab'}
    // onPress={() => {
      // this.setState({
        // selectedTab: 'editTab',
      // });
      // window.location.hash='/hfive/editcl';
    // }}
    // data-seed="logId"
  // >
  // </TabBar.Item>
// </TabBar>
export default connect()(Clues)
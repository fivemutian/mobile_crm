import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Card,Tabs } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
import {reqwest_post,reqwest_get} from '../../utils/Reqwest.js';
const Item = List.Item;
const Brief = Item.Brief;
const TabPane = Tabs.TabPane;
class Clues extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false,
      tab:'1'
		}
	}
	loadServer(params={} ) {
    console.log(params);
    let _data=[];
    let _this=this;
    // let _data=[];
    let _tab = null;
    this.setState({ animating: true });
    this.state.tab==='1'||params.tab==="1"?_tab={
      access_token:localStorage['token'],
      page:params,
      pre:5
    }:_tab={
      access_token:localStorage['token'],
      page:params,
      type:'assign',
      pre:5
    }
    reqwest({
      url: window.host+'/api/clues',
      method: 'get',
      crossOrigin: true,
      data: _tab,
      type: 'json',
    }).then((data) => {
      const pagination = { ...this.state.pagination };
      pagination.total = data.total;
      this.setState({
        animating: false,
        // data: _data,
        list:data.list,
        pagination:pagination.total,
        // pageCurrent
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
  }
  componentWillMount(){
  	this.loadServer(1)
    // alert(localStorage['token'])
  	// if(window.location.host=="172.16.0.249:8181"){
  	// 	// sessionStorage['token']="9aySPxzAmrO2xwg/PKFFKyOR/Tz3AB7OsmxH2kRUn/S2X3HFwtv386zl3gRB8nOkUdo4zUsqepccrCe1BgPrTA=="
  	// }

    // window.sessionStorage['new']='/hfive/newcl';
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editcl","text":"编辑"},{"url":"del","text":"删除","method":"/api/clues/"}]);
    // window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editcl","text":"编辑"}]);
  }
  componentWillUnmount(){
  }
  handelPageChage(e,tar){
    console.log(e)
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
  handelTabsChage(e){
    this.state.tab=e;
    // this.state.pageCurrent=1
    this.setState({pageCurrent:0})
    console.log(this.state.tab)
    this.loadServer(1)
  }
	render(){
		const ItemList = this.state.list.map(function(data,index){
			return <Link onClick={()=>{window.sessionStorage['ism']=JSON.stringify(data)}} key={index} to={{pathname:'/hfive/infocl',query:{ism:JSON.stringify(data)}}}><Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        key={data.id}
        className={`${Reset.tel_list}`}
        platform="android"
      >
        姓名:{data.name}<Brief>电话:{data.mobile}</Brief>
      </Item></Link>
		})
		return(
			<div className="flex-container">
        <Tabs className={`${Reset.reset_tabs}`} defaultActiveKey="1" onChange={(e)=>{this.handelTabsChage(e)}} onTabClick={(e)=>{console.log(e)}}>
          <TabPane  tab="我的线索" key="1">
            <List   className={`${Reset.tel_list_header}`}>
              {this.state.list.length<1?<Item
                  className={`${Reset.tel_list}`}
                  arrow="horizontal"
                  multipleLine
                  onClick={() => {}}
                  platform="android"
                  className={`${style.dislike}`}
                >
                  <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
                  <Brief>暂无数据</Brief>
               </Item>:ItemList}
              <ActivityIndicator
                toast
                text="正在加载"
                animating={this.state.animating}
              />
            </List>
          <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/10)} 
            locale={{
              prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
              nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
            }} />
          </TabPane>
          <TabPane  tab="指派给我" key="2">
            <List   className={`${Reset.tel_list_header}`}>
              {this.state.list.length<1?<Item
                  className={`${Reset.tel_list}`}
                  arrow="horizontal"
                  multipleLine
                  onClick={() => {}}
                  platform="android"
                  className={`${style.dislike}`}
                >
                  <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
                  <Brief>暂无数据</Brief>
               </Item>:ItemList}
              <ActivityIndicator
                toast
                text="正在加载"
                animating={this.state.animating}
              />
            </List>
            <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/10)} 
              locale={{
                prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
                nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
              }} />
          </TabPane>
        </Tabs>
        
			</div>
		)
	}
}
export default connect()(Clues)
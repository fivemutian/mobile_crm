import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace} from 'antd-mobile';
import {Rate} from 'antd'
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
class ClueCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
      pound:0
		}
	}
	loadServer(params={}){
		// console.log(data)
    let _this = this;
		reqwest({
      url: window.host+'/api/clues',
      method: 'post',
      crossOrigin: true,
      type: 'json',
      data:{ access_token:localStorage['token'],
         clue:{name:params.contact,mobile:params.phone.replace(/\s+/g, ""),address:params.address,pound:_this.state.pound,remark:JSON.stringify(params.remark)},
      }
    }).then((data) => {
      if(data.status=='success'){
          Toast.success(
          '保存成功',
          1,
          ()=>{
            // window.localStorage['token']=data.token;
            // window.localStorage['curloe']=data.current_user.role;
            // // debugger
            // window.localStorage['menu']=JSON.stringify(data.menu);
            // window.localStorage['cgj_id']=data.current_user.cgj_user_id;
            // window.localStorage['account_type']=data.current_user.account_type;
            // window.localStorage['userrr_id']=data.current_user.id;
            window.location.hash='/hfive/clues';
          }
        )
        
      }else{
        Toast.fail(
          data.msg,
          1
        )
      }
    }).fail( (err, msg) =>{
       Toast.offline('网络连接失败!!!', 1);
    });
	}
	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        console.log(values)
        // return;
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
      if (!err  ) {
        console.log('Received values of form: ', values);
      	_this.loadServer(values)
      }
    });
	}
	componentWillMount(){
		let _regions=[];
		if(!!sessionStorage['regions']){
			JSON.parse(sessionStorage['regions']).map(function(data,index){
				_regions.push({
					value:data.id,
					label:data.name
				})
			})
		}
		this.setState({reginos:_regions})
	}
	render(){
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
		 return(
			<div className={`${style.layout}`}>
				<List className={`${Reset.tel_list_header}`} renderHeader={() => '线索'}>
          <InputItem
	          clear
            className={`${Reset.tel_input}`}
	          error={!!getFieldError('contact')}
	          onErrorClick={() => {
	            alert(getFieldError('contact').join('、'));
	          }}
            {...getFieldProps('contact',{
            	rules:[{required:true,message:'客户姓名'}]
            })}
            placeholder="客户姓名"
          >联系人</InputItem>
          <InputItem
	          clear
            className={`${Reset.tel_input}`}
	          error={!!getFieldError('phone')}
	          onErrorClick={() => {
	            alert(getFieldError('phone').join('、'));
	          }}
            {...getFieldProps('phone',{
            	rules:[{required:true,message:'请输入手机号码',validator:(rule,value,callback)=>{
								!!value?numvail(rule,value.replace(/\s+/g, ""),callback):callback('123')
							}}]
            })}
            type='phone'
            placeholder="手机号码"
          >手机号码</InputItem>
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('address')}
            onErrorClick={() => {
              alert(getFieldError('address').join('、'));
            }}
            {...getFieldProps('address',{
              rules:[{required:true,message:'请输入地址'}]
            })}
            placeholder='地址'
          >地址</InputItem>
          <List.Item
            className={`${Reset.tel_input}`}
            {...getFieldProps('pound',{
            	rules:[{message:'优先级'}]
            })}
            placeholder='地址'
          >优先级<Rate style={{fontSize:'0.4rem',marginLeft:'0.7rem'}} onChange={(e)=>this.setState({pound:e})} allowHalf={false} /></List.Item>
          <List className={`${Reset.tel_list_header}`} renderHeader={() => '备注'} style={{background:'#f5f5f9'}}>
	          <Editor {...getFieldProps('remark', {
                // initialValue: '备注'
              })}
             toolbarHidden editorStyle={{fontSize:'0.25rem',margin:0}} wrapperStyle={{border:'1px solid #fcfcfc',borderRadius:4,width:'100vw',minHeight:'2rem'}} />
	        </List>
        </List>
        <Button className={`${Reset.btn_moblie} ${Reset.tel_btn}`} onClick={this.handleSubmit.bind(this)}  type='primary'>保存</Button>
        
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}


const ClueForm = createForm()(ClueCreate);
export default connect()(ClueForm)
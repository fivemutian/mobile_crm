import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace,ImagePicker} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import setStyle from '../../style/Set.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import PropTypes from 'prop-types';
class AccountCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
      files:[],
      filesList:[]
		}
	}
	loadServer(params={}){
		// console.log(data)
    const {fileList,files } =this.state;
		reqwest({
      url: window.host+'/api/accounts/add_company',
      method: 'post',
      crossOrigin: true,
      type: 'json',
      data:{ access_token:localStorage['token'],
        company:{name:params.name,logo:files[0].url},
        admin_id:window.localStorage['userrr_id'],
        admin:{mobile:params.phone.replace(/\s+/g, ""),name:params.username},
      }
    }).then((data) => {
      if(data.status=='success'){
          Toast.success(
          '保存成功',
          1,
          ()=>{
            // window.localStorage['token']=data.token;
            // window.localStorage['curloe']=data.current_user.role;
            // // debugger
            // window.localStorage['menu']=JSON.stringify(data.menu);
            // window.localStorage['cgj_id']=data.current_user.cgj_user_id;
            // window.localStorage['account_type']=data.current_user.account_type;
            // window.localStorage['userrr_id']=data.current_user.id;
            window.location.hash='/hfive/Account';
          }
        )
        
      }else{
        Toast.fail(
          data.msg,
          1
        )
      }
    }).fail( (err, msg) =>{
       Toast.offline('网络连接失败!!!', 1);
    });
	}
	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
        // console.log(values.phone)
        // return
      if (!err  ) {
        console.log('Received values of form: ', values);
      	_this.loadServer(values)
      }
    });
	}
	componentWillMount(){
		let _regions=[];
		if(!!sessionStorage['regions']){
			JSON.parse(sessionStorage['regions']).map(function(data,index){
				_regions.push({
					value:data.id,
					label:data.name
				})
			})
		}
		this.setState({reginos:_regions})
	}
  onChange (files, type, index) {
    console.log(files, type, index);
    this.setState({
      files,
    });
  }
  handleImageClick(index,fs){
    this.onChange.bind(this);
  }
	render(){
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
		 return(
			<div className={`${style.layout}`}>
        <List className={`${Reset.tel_list_header} ${Reset.img_picker}`} renderHeader={() => 'logo'} style={{background:'#f5f5f9'}}>
          <ImagePicker
            className={`${setStyle.cha}`}
            files={this.state.files}
            onChange={this.onChange.bind(this)}
            onImageClick={this.handleImageClick.bind(this)}
            selectable={this.state.files.length < 1}
          />
          <ImagePicker
              files={this.state.filesList}
              className={`${setStyle.set_img}`}
              onChange={this.onChange.bind(this)}
              selectable={this.state.filesList.length < 1}
            />
        </List>
				<List className={`${Reset.tel_list_header}`} renderHeader={() => '品牌'}>
          <InputItem
            clear
            className={`${Reset.tel_input}`}
            error={!!getFieldError('name')}
            onErrorClick={() => {
              alert(getFieldError('name').join('、'));
            }}
            {...getFieldProps('name',{
              rules:[{required:true,message:'品牌名称'}]
            })}
            placeholder='品牌名称'
          >品牌名称</InputItem>
          <InputItem
	          clear
            className={`${Reset.tel_input}`}
	          error={!!getFieldError('username')}
	          onErrorClick={() => {
	            alert(getFieldError('username').join('、'));
	          }}
            {...getFieldProps('username',{
            	rules:[{required:true,message:'管理员姓名'}]
            })}
            placeholder="管理员姓名"
          >管理员姓名</InputItem>
          <InputItem
	          clear
            className={`${Reset.tel_input}`}
	          error={!!getFieldError('phone')}
	          onErrorClick={() => {
	            alert(getFieldError('phone').join('、'));
	          }}
            {...getFieldProps('phone',{
            	rules:[{required:true,message:'请输入手机号码',len:11,validator:(rule,value,callback)=>{
								!!value?numvail(rule,value.replace(/\s+/g, ""),callback):callback('请输入手机号码')
							}}]
            })}
            type='phone'
            placeholder="管理员手机"
          >管理员手机</InputItem>

          
        </List>
        <Button className={`${Reset.btn_moblie} ${Reset.tel_btn}`} onClick={this.handleSubmit.bind(this)}  type='primary'>保存</Button>
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}

const AccountForm = createForm()(AccountCreate);
export default connect()(AccountForm)
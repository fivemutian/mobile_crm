import React from 'react';
import { connect } from 'dva';
import { Flex, List, Toast,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Checkbox,Button } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
const CheckboxItem = Checkbox.CheckboxItem;
const AgreeItem = Checkbox.AgreeItem;
function removeByValue(arr, val) {
  for(var i=0; i<arr.length; i++) {
    if(arr[i] == val) {
      arr.splice(i, 1);
      break;
    }
  }
}
Array.prototype.indexOf = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    }
    return -1;
};
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Array.prototype.unique1 = function(){
 var res = [this[0]];
 for(var i = 1; i < this.length; i++){
  var repeat = false;
  for(var j = 0; j < res.length; j++){
   if(this[i] == res[j]){
    repeat = true;
    break;
   }
  }
  if(!repeat){
   res.push(this[i]);
  }
 }
 return res;
}
class Apply extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false,
      pagination:1,
      apply:[]
		}
	}
  fetch (params = {}) {
    console.log('params:', this.state.apply);
    let _data=[];
    let _this = this;
    reqwest({
      url: window.host+'/api/accounts/apply_co_companies',
      method: 'post',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        company_ids:_this.state.apply,
      },
      type: 'json',
    }).then((data) => {
      if(data.status=='success'){
        Toast.success('关联品牌成功', 0.4, ()=>{
          window.location.hash='/hfive/dealer'
        }, true)
      }
    });
  }
  componentWillMount(){
    window.sessionStorage['new']='/hfive/newapply';
  }
  hangdleChagen(e){
    let _arr = this.state.apply;
    if(e.target.checked){
      _arr.push(e.target.value);
      this.setState({apply:_arr});
      console.log(this.state.apply.unique1())
    }else{
      _arr.remove(e.target.value)
      this.setState({apply:_arr});
    }
    
  }
	render(){
    console.log(this.state.list.length)
		return(
			<div className="flex-container">
      	<List className={`${Reset.tel_list_header}`} renderHeader={() => '关联品牌'}>
          {this.state.list.length<1?<Item
              arrow="horizontal"
              multipleLine
              onClick={() => {}}
              platform="android"
              className={`${style.dislike}`}
            >
              <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
              <Brief>暂无数据</Brief>
           </Item>:null}<Flex>
            <Flex.Item>
              {JSON.parse(window.localStorage['select_list']).map((i,index) => (
                <AgreeItem value={i.id}  key={i.id} data-seed={index} className={`${Reset.label} ${Reset.tel_radio}`} onChange={(e)=>{this.hangdleChagen(e)}}>
                  {i.name}
                  <Brief >  </Brief>
                </AgreeItem>
              ))}
            </Flex.Item>
          </Flex>
           </List>
          <Button className={`${Reset.btn_moblie}  ${Reset.tel_btn}`} onClick={this.fetch.bind(this)}  type='primary'>保存</Button>
       
			</div>
		)
	}
}
//<img src={i.logo} />
export default connect()(Apply)
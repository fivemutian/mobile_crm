import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
class Account extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false,
      pagination:1
		}
	}
	loadServer(params={} ) {
    // alert(params);
    let _data=[];
    let _this=this;
    this.setState({ animating: true });
    reqwest({
      url: window.host+'/api/accounts/companies',
      method: 'get',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        page:params,
        pre:5
      },
      type: 'json',
    }).then((data) => {
      // alert(data.status);
      // localStorage['regions']=JSON.stringify(data.regions);
      const pagination = { ...this.state.pagination };
      pagination.total = data.total;
      this.setState({
        animating: false,
        list:data.list,
        pagination:pagination.total,
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
  }
  componentWillMount(){
  	this.loadServer(1)
    window.sessionStorage['new']='/hfive/newan';
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
	render(){
		const ItemList = this.state.list.map(function(data,index,len){
			return <Item
        arrow="horizontal"
        multipleLine
        className={`${Reset.tel_list}`}
        onClick={() => {}}
        key={data.id}
        platform="android"
      >
        品牌:{data.name}<Brief>管理员:{data.admin_name}</Brief><Brief>手机号:{data.admin_mobile}</Brief>
      </Item>
		})
    console.log(this.state.list.length)
		return(
			<div className="flex-container">
        <List renderHeader={() => '品牌'}  className={`${Reset.tel_list_header}`}>
      	  {this.state.list.length<1?<Item
			        arrow="horizontal"
			        multipleLine
			        onClick={() => {}}
			        platform="android"
			        className={`${style.dislike}`}
			      >
			        <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
			        <Brief>暂无数据</Brief>
     			 </Item>:ItemList}
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={this.state.pagination==undefined?1:Math.ceil(this.state.pagination/5)} 
		      locale={{
		        prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
		        nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
		      }} />
			</div>
		)
	}
}
export default connect()(Account)
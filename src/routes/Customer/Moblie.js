import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import {Link }from 'dva/router'
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import {Button,Input, Progress} from 'antd'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
class Customer extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false
		}
	}
	loadServer(params = {}) {
    console.log('params:', params);
    let _data=[];
    let _this=this;
    this.setState({ animating: true });
    reqwest({
      url: window.host+'/api/customers',
      method: 'get',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        page:params,
        per_page:5
      },
      type: 'json',
    }).then((data) => {
      console.log(data);
      const pagination = { ...this.state.pagination };
      pagination.total = data.total;
      this.setState({
        animating: false,
        // data: _data,
        list:data.list,
        pagination:pagination.total,
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillMount(){
  	this.loadServer(1)
    window.sessionStorage['new']='/hfive/newcus';
    window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editcus","text":"编辑"}]);
  }
  componentWillUnmount(){
    
  }
  
	render(){
		const ItemList = this.state.list.map(function(data){
			return <Link key={data.id} onClick={()=>{window.sessionStorage["ism"]=JSON.stringify(data)}} to={{pathname:'/hfive/infocus'}}><Item
        arrow="horizontal"
        className={`${Reset.tel_list}`}
        multipleLine
        onClick={() => {}}
        platform="android"
      >
        姓名:{data.name}<Brief>电话:{data.tel}</Brief>
      </Item></Link>
		})
		return(
			<div className="flex-container">
        <List renderHeader={() => '客户详情'}  className={`${Reset.tel_list_header}`}>
      	  {this.state.list.length<1?<Item
              arrow="horizontal"
              multipleLine
              onClick={() => {}}
              platform="android"
              className={`${style.dislike}`}
            >
              <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
              <Brief>暂无数据</Brief>
           </Item>:ItemList}
           <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/10)} 
		      locale={{
		        prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
		        nextText: (<div  className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
		      }} />
			</div>
		)
	}
}
export default connect()(Customer)
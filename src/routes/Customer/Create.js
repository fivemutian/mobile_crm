import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
class CustomerCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
			loading:false
		}
	}
	loadServer(params={}){
		// console.log(data)
		const _this = this;
		this.setState({loading:true});
		reqwest({
      url: window.host+'/api/customers',
      method: 'post',
      crossOrigin: true,
      type: 'json',
      data:{ access_token:localStorage['token'],
      customer:{name:params.name,tel:params.phone,province:params.province[0],city:params.province[1],area:params.province[2],street:params.address,remark:params.remark}
    }
    }).then((data) => {
      if(data.status=='success'){
          Toast.success(
          '保存成功',
          1,
          ()=>{
            window.location.hash='/hfive/customers';
          }
        )
        
      }else{
      	this.setState({loading:false});
        Toast.fail(
          data.msg,
          1
        )
      }
    }).fail( (err, msg) =>{
    	this.setState({loading:false});
       Toast.offline('网络连接失败!!!', 1);
    });
	}
	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
        console.log(values.province)
      if (!err  ) {
        console.log('Received values of form: ', values);
      	_this.loadServer(values)
      }
    });
	}
	componentWillMount(){
		let _regions=[];
	}
	render(){
		console.log(this.state.list)
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
		 return(
			<div className={`${style.layout}`}>
				<List className={`${Reset.tel_list_header}`} renderHeader={() => '新建客户'}>
          <InputItem
	          clear
	          className={`${Reset.tel_input}`}
	          error={!!getFieldError('name')}
	          onErrorClick={() => {
	            alert(getFieldError('name').join('、'));
	          }}
            {...getFieldProps('name',{
            	rules:[{required:true,message:'姓名'}]
            })}
            placeholder="姓名"
          >姓名</InputItem>
          <InputItem
	          clear
	          className={`${Reset.tel_input}`}
	          error={!!getFieldError('phone')}
	          onErrorClick={() => {
	            alert(getFieldError('phone').join('、'));
	          }}
            {...getFieldProps('phone',{
            	rules:[{required:true,message:'请输入手机号码',len:11,validator:(rule,value,callback)=>{
								!!value?numvail(rule,value.replace(/\s+/g, ""),callback):callback('请输入手机号码')
							}}]
            })}
            type='phone'
            placeholder="手机号码"
          >手机号码</InputItem>
          <Picker   extra={<span>地址</span>} style={{textAlign:'left'}} clear
          		className={`${Reset.tel_picker}`}
	          	error={!!getFieldError('province')}
		          onErrorClick={() => {
		            alert(getFieldError('province').join('、'));
		          }}
	            placeholder="选择地址"
		          onOk={(e)=>{this.setState({disable:false});console.log(e)}}
		          {...getFieldProps('province',{
		          	rules:[{required:true,message:'请选择地址'}]
		          })}  title="选择地址" data={options} >
	          <List.Item arrow="horizontal"
	          	className={`${Reset.list_extra} ${Reset.tel_input}`}
	          >选择地址</List.Item>
	        </Picker>
          <InputItem
	          clear
	          className={`${Reset.tel_input}`}
	          error={!!getFieldError('address')}
	          onErrorClick={() => {
	            alert(getFieldError('address').join('、'));
	          }}
            {...getFieldProps('address',{
            	rules:[{required:true,message:'请输入详细地址'}]
            })}
            placeholder='详细地址'
          >详细地址</InputItem>
          <List className={`${Reset.tel_list_header}`} renderHeader={() => '备注'} style={{background:'#f5f5f9'}}>
	          <TextareaItem
	          className={`${Reset.tel_textarea}`}
	            {...getFieldProps('remark', {
	              initialValue: '备注'
	            })}
	            rows={5}
	            count={100}
	          />
	        </List>
        </List>
        <Button loading={this.state.loading} className={`${Reset.btn_moblie} ${Reset.tel_btn}`} onClick={this.handleSubmit.bind(this)}  type='primary'>保存</Button>
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}

const CustomerForm = createForm()(CustomerCreate);
export default connect()(CustomerForm)
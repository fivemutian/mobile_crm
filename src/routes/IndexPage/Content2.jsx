import React from 'react';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
// import style from './less/content1.less';
import style from './less/antMotion_style.less';
class Content extends React.Component {

  static defaultProps = {
    className: 'content1',
  };

  render() {
    const props = { ...this.props };
    const isMode = props.isMode;
    delete props.isMode;
    const animType = {
      queue: isMode ? 'bottom' : 'left',
      one: isMode ? { y: '+=30', opacity: 0, type: 'from' }
        : { x: '+=30', opacity: 0, type: 'from' },
    };
    return (
      <div
        {...props}  
        className={`${style.content_template_wrapper} ${style.content_half_wrapper} ${style.content1_wrapper}`}
      >
        <OverPack
          className={`${style.content_template} ${style.content1}`}
          location={props.id}
        >
          <TweenOne
            key="img"
            animation={animType.one}
            className={`${style.content1_img}`}
            id={`${props.id}-imgWrapper`}
            resetStyleBool
          >
            <span id={`${props.id}-img`}>
              <img  src="https://zos.alipayobjects.com/rmsportal/tvQTfCupGUFKSfQ.png" />
            </span>
          </TweenOne>
          <QueueAnim
            type={animType.queue}
            className={`${style.content1_text}`}
            key="text"
            leaveReverse
            ease={['easeOutCubic', 'easeInCubic']}
            id={`${props.id}-textWrapper`}
          >
            <h1 key="h1" id={`${props.id}-title`}>
              销售
            </h1>
            <p key="p" id={`${props.id}-content`}>
              借助全球首屈一指的 CRM 解决方案，您的销售团队能够利用一系列基于云的工具，以前所未有的方式完成交易，这些工具可提高工作效率，为销售漏斗填充可靠的销售线索，并提高成功率。 无需软件。 无需硬件。 无速度限制。
            </p>
          </QueueAnim>
          
        </OverPack>
      </div>
    );
  }
}

export default Content;

import React from 'react';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import QueueAnim from 'rc-queue-anim';
import style from './less/antMotion_style.less';

class Footer extends React.Component {
  static propTypes = {
    id: React.PropTypes.string,
  };

  static defaultProps = {
    className: 'footer1',
  };

  getLiChildren = (data, i) => {
    const links = data.contentLink.split(/\n/).filter(item => item);
    const content = data.content.split(/\n/).filter(item => item)
      .map((item, ii) => {
        const cItem = item.trim();
        const isImg = cItem.match(/\.(jpg|png|svg|bmp|jpeg)$/i);
        return (<li className={isImg ? style.icon : ''} key={ii}>
          <a href={links[ii]} target="_blank">
            {isImg ?<img width="60" height="60" src={require('../../assets/crmlog.png')}  /> : cItem}
          </a>
        </li>);
      });
      return (<li className={data.className}  key={i} id={`${this.props.id}-block${i}`}>
        <h2>{data.title}</h2>
        <ul>
          {content}
        </ul>
      </li>);
  }

  render() {
    const props = { ...this.props };
    const isMode = props.isMode;
    delete props.isMode;
    // const logoContent = { img: "/static/crmlog.4dbbe816.png", content: '管家CRM' };
    const dataSource = [
      { title: '指南', content: '新手上路\n操作指南', contentLink: '#\n#\n#\n#' },
      { title: '产品', content: '产品介绍\n案例', contentLink: '#\n#\n#\n#' },
      { title: '关于', content: '关于我们\n品牌故事\n练习我们', contentLink: '#\n#' },
      { title: '资源', content: '加入我们\n企业加盟\n管家CRM\n管家CRM', contentLink: '#\n#\n#\n#' },
      // { title: '关注', content: 'https://zos.alipayobjects.com/rmsportal/IiCDSwhqYwQHLeU.svg\n https://zos.alipayobjects.com/rmsportal/AXtqVjTullNabao.svg\n https://zos.alipayobjects.com/rmsportal/fhJykUTtceAhYFz.svg\n https://zos.alipayobjects.com/rmsportal/IDZTVybHbaKmoEA.svg', contentLink: '#\n#\n#\n#' },
    ];
        // {liChildrenToRender}
    // console.log(props)
    // const liChildrenToRender = dataSource.map(this.getLiChildren);
    return (<div
      {...props} className={style.footer1}
       style={{height:'0.6rem'}}
    >
      <div
        style={{height:'0.6rem'}}
        key="copyright"
        className={`${style.copyright}`}
        id={`${props.id}-content`}
      >
        <span>
          © 2016 - All Right with 5mutian <a href="http://www.miitbeian.gov.cn" target='_blank'  >沪ICP备16051307号</a>
        </span>
      </div>      
     
    </div>);
  }
}
// playScale={3} animation={{ y: '+=30', opacity: 0, type: 'from' }}
// <QueueAnim type="bottom" component="ul" key="ul" leaveReverse id={`${props.id}-ul`}>
      //   <li key="logo" id={`${props.id}-logo`}>
      //     <p className={`${style.logo}`}>
      //       <img src={require('../../assets/crmlog.png')} width="30" height='30' />
      //     </p>
      //     <p><span>
      //     © 2016 - All Right with 5mutian <a href="http://www.miitbeian.gov.cn" target='_blank'  >沪ICP备16051307号</a>
      //   </span></p>
      //   </li>
      // {liChildrenToRender}
      // </QueueAnim>
 
export default Footer;

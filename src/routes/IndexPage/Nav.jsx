import React, { PropTypes } from 'react';
import TweenOne from 'rc-tween-one';
import { Menu } from 'antd';
import style from './less/nav.less';
import {  Link} from 'dva/router';

const Item = Menu.Item;

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneOpen: false,
      current:'1'
    };
  }

  phoneClick = () => {
    this.setState({
      phoneOpen: !this.state.phoneOpen,
    });
  }
  componentWillMount(){
    console.log(this.props)
  }
  render() {
    console.log(this.props.isMode);
    const props = { ...this.props };
    const {current,phoneOpen} =this.state
    const isMode = props.isMode;
    delete props.isMode;
    const ccc=this.props.className;
    const aaa=style.ccc
    const navData = { '/': '首页', '/log': '登录' };;
    const navChildren = Object.keys(navData)
      .map((key, i) => (<Item style={{fontSize:'0.28rem',height:'auto',padding:'0.24rem'}} key={i}> <Link to={{pathname:key}}><span>{navData[key]}</span></Link></Item>));
    return (<TweenOne
      component="div"
      animation={{ opacity: 0, type: 'from' }}
      {...props}
    >
      <TweenOne style={{position:'relative',marginLeft:'0.33rem'}}
        className={`${style.header0_logo}`}
        animation={{ x: -30, type: 'from', ease: 'easeOutQuad' }}
        id={`${this.props.id}-logo`}
      >
        <Link to={{pathname:'/home'}}><img  src={require('../../assets/crmlog.png')}  />管家快销</Link>
      </TweenOne>
      <TweenOne
        className={`${style.header0_nav}`} style={{marginTop:'0.1rem'}}
        animation={{ x: 30, type: 'from', ease: 'easeOutQuad' }}
      >
        <Menu
          mode="horizontal" defaultSelectedKeys={[current]}
          id={`${this.props.id}-menu`}
          theme="dark"
          style={{background:'none',border:'none'}}
          onClick={(e,key)=>{this.setState({current:key})}}
        >
          {navChildren}
        </Menu>
      </TweenOne>
    </TweenOne>);
  }
}

Header.propTypes = {
  className: PropTypes.string,
  dataSource: PropTypes.object,
  id: PropTypes.string,
};

Header.defaultProps = {
  className: 'header0',
};
      // {isMode ? (<div
      //   className={`${style.header0_phone_nav} ${this.state.phoneOpen ? ' open' : ''}`}
      //   id={`${this.props.id}-menu`}
      // >
      //   <div
      //     className={`${style.header0_phone_nav_bar}`}
      //     onClick={
      //       this.phoneClick.bind(this)
      //     }
      //   >
      //     <em />
      //     <em />
      //     <em />
      //   </div>
      //   <div
      //     className={`${style.header0_phone_nav_text}`} style={{opacity:phoneOpen?1:0}}
      //   >
      //     <Menu
      //       defaultSelectedKeys={['0']}
      //       mode="inline"
      //       theme="dark"
      //       onTitleClick={()=>{alert(1)}}

      //       onClick={()=>{console.log(1)}}
      //       style={{marginTop:'3%'}}
      //     >
      //       {navChildren}
      //     </Menu>
      //   </div>
      // </div>) : (<TweenOne
      //   className={`${style.header0_nav}`} style={{marginTop:'0.1rem'}}
      //   animation={{ x: 30, type: 'from', ease: 'easeOutQuad' }}
      // >
      //   <Menu
      //     mode="horizontal" defaultSelectedKeys={[current]}
      //     id={`${this.props.id}-menu`}
      //     theme="dark"
      //     onClick={(e,key)=>{this.setState({current:key})}}
      //   >
      //     {navChildren}
      //   </Menu>
      // </TweenOne>)}
export default Header;

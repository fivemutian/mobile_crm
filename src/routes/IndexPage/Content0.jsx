import React, { PropTypes } from 'react';
import { Button, Icon } from 'antd';
import {Link} from 'dva/router'
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

import style from './less/antMotion_style.less';
class Content extends React.Component {
  render() {
    const props = { ...this.props };
    delete props.isMode;
    return (
      <OverPack
        replay
        playScale={[0.3, 0.1]}
        {...props} style={{position:'relative'}}
        className={`${style.banner0}`}
      >
        <QueueAnim
          type={['bottom', 'top']}
          delay={200}
          className={`${style.banner0_wrapper}`}
          key="text"
          id={`${props.id}-wrapper`}
        >
          <span
            className={`${style.title}`}
            key="title"
            id={`${props.id}-title`}
          >
            <img  src={require('../../assets/banner.png')}  />
          </span>
          <p
            key="content"
            id={`${props.id}-content`}
          >
            使用全球智能化管家CRM平台，实现销售流程可视化管理 解决家装产品销售的烦恼！ 让老板放心，员工省心，客户安心
          </p>
          <Button className={`${style.btn}`} type="ghost" key="button" id={`${props.id}-button`}>
            <Link to={{pathname:'/reg'}}>加入我们</Link>
          </Button>
        </QueueAnim>
        <TweenOne
          animation={{ y: '-=20', yoyo: true, repeat: -1, duration: 1000 }}
          className={`${style.banner0_icon}`}
          key="icon"
        >
          <Icon type="down" />
        </TweenOne>
      </OverPack>
    );
  }
}

Content.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
};

Content.defaultProps = {
  className: 'banner0',
};

export default Content;

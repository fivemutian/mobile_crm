import React from 'react';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import style from './less/antMotion_style.less';
class Content extends React.Component {

  static defaultProps = {
    className: 'content2',
  };

  getDelay = e => e % 3 * 100 + Math.floor(e / 3) * 100 + 300;

  render() {
    const props = { ...this.props };
    delete props.isMode;
    const oneAnim = { y: '+=30', opacity: 0, type: 'from', ease: 'easeOutQuad' };
    const blockArray = [
      { icon: 'https://zos.alipayobjects.com/rmsportal/ScHBSdwpTkAHZkJ.png', title: '简单实用、功能齐全', content: '客户管理、销售管理、订单管理、一应用俱全；灵活的个性化配置，让员工更愿用' },
      { icon: 'https://zos.alipayobjects.com/rmsportal/NKBELAOuuKbofDD.png', title: '节省成本、效率提高', content: '集合管家CRM系统、家管家服务系统，一个平台、无限量应用模块;销售管理数据分析、报表统计图，清晰明了随时可看；' },
      { icon: 'https://zos.alipayobjects.com/rmsportal/xMSBjgxBhKfyMWX.png', title: '流程把控、步步为赢', content: '优化销售管理过程，提高销售业绩。' },
      { icon: 'https://zos.alipayobjects.com/rmsportal/MNdlBNhmDBLuzqp.png', title: '数据时效、响应变化', content: '适应市场不断变化的需求，及时跟进客户的真实需求，并提供有效的服务；同时根据销售数据帮助销售管理者制定相应的销售策略' },
      { icon: 'https://zos.alipayobjects.com/rmsportal/UsUmoBRyLvkIQeO.png', title: '直销、代销相结合', content: '拥有一套完善的解决方案，将服务延伸到客户终端；' },
      // { icon: 'https://zos.alipayobjects.com/rmsportal/ipwaQLBLflRfUrg.png', title: '大数据', content: '管家CRM、管家CRM、管家CRM、管家CRM、管家CRM、管家CRM、管家CRM。' },
    ];
    const children = blockArray.map((item, i) => {
      const id = `block${i}`;
      const delay = this.getDelay(i);
      const liAnim = { opacity: 0, type: 'from', ease: 'easeOutQuad', delay };
      const childrenAnim = { ...liAnim, x: '+=10', delay: delay + 100,};
      return (<TweenOne
        component="li"
        animation={liAnim}
        key={i} 
        id={`${props.id}-${id}`}
      >
        <TweenOne
          animation={{ x: '-=10', opacity: 0, type: 'from', ease: 'easeOutQuad' }}
          className={`${style.img}`}
          key="img"
        >
          <img src={item.icon} width="100%" />
        </TweenOne>
        <div className={`${style.text}`}>
          <TweenOne key="h1" animation={childrenAnim} component="h1">
            {item.title}
          </TweenOne>
          <TweenOne key="p" animation={{ ...childrenAnim, delay: delay + 200 }} component="p">
            {item.content}
          </TweenOne>
        </div>
      </TweenOne>);
    });
    return (
      <div {...props} 
      className={`${style.content_template_wrapper} ${style.content2_wrapper}`}>
        <OverPack
          className={`${style.content_template} ${style.content2}`}
          location={props.id}
        >
          <TweenOne
            key="h1"
            animation={oneAnim}
            component="h1"
            id={`${props.id}-title`}
            reverseDelay={100}
          >
             管家快销能做什么
          </TweenOne>
          <QueueAnim
            key="ul"
            type="bottom"
            className={`${style.content2_contentWrapper}`}
            id={`${props.id}-contentWrapper`}
          >
            <ul key="ul">
              {children}
            </ul>
          </QueueAnim>
        </OverPack>
      </div>
    );
  }
}


export default Content;

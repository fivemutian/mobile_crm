import React from 'react';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import style from './less/antMotion_style.less';
class Content extends React.Component {
  static defaultProps = {
    className: 'content0',
  };

  render() {
    const props = { ...this.props };
    const isMode = props.isMode;
    delete props.isMode;
    const animType = {
      queue: isMode ? 'bottom' : 'right',
      one: isMode ? { y: '+=30', opacity: 0, type: 'from' }
        : { x: '-=30', opacity: 0, type: 'from' },
    }
    return (
      <div
        {...props} style={{position:'relative', background:'#fff'}}
        className={` ${style.content_half_wrapper} ${style.content0_wrapper}`}
      >
        <OverPack
          className={`${style.content_template} ${style.content0}`}
          location={props.id}
        >
          <TweenOne
            key="img"
            animation={animType.one}
            className={`${style.content0_img}`}
            id={`${props.id}-imgWrapper`}
            resetStyleBool
          >
            <span id={`${props.id}-img`}>
              <img  src="https://zos.alipayobjects.com/rmsportal/nLzbeGQLPyBJoli.png" />
            </span>
          </TweenOne>
          <QueueAnim
            className={`${style.content0_text}`}
            type={animType.queue}
            key="text"
            leaveReverse
            ease={['easeOutCubic', 'easeInCubic']}
            id={`${props.id}-textWrapper`}
          >
            <h1 key="h1" id={`${props.id}-title`}>
              服务
            </h1>
            <p key="p" id={`${props.id}-content`}>
              我们专注于提升销售业绩，客户满意度
大数据时代的销售管理神器
OR产品销售管理简单实用
提高销售业绩，客户满意度的不二选择
            </p>
          </QueueAnim>
        </OverPack>
      </div>
    );
  }
}


export default Content;

import React from 'react';
import ReactDOM from 'react-dom';
import enquire from 'enquire.js';
import { scrollScreen } from 'rc-scroll-anim';

import Nav from './Nav';
import Content0 from './Content0';
import Content1 from './Content1';
import Content2 from './Content2';
import Content3 from './Content3';
import Footer from './Footer';

import style from './less/antMotion_style.less';
import reqwest  from 'reqwest'

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMode: false,
    };
  }
  componentWillMount(){
    window.localStorage.clear()
    if(window.localStorage['token']!=undefined){
      window.location.hash='/hfive/home'
    }
    // let c =window.location.hash;
    // let _this = this;
    // // alert(navigator)

    // if(c.match(/code/)==['code']||c.match(/code/)=="code"){
    //   let a =window.location.hash.match(/code\=\w+/);
    //   let b = a[0].match(/\w+/g);
    //   reqwest({
    //     url: 'http://172.16.0.248:3000/wechat/auth/login',
    //     method: 'post',
    //     crossOrigin: true,
    //     data:{
    //       code:b[1]
    //     },
    //     type: 'json',
    //   }).then((data)=>{
    //     console.log(data);
    //   }).fail((data)=>{
    //      console.log(data);
    //   })
    // }
  }
  componentDidMount() {
    // 适配手机屏幕;
    this.enquireScreen((isMode) => {
      this.setState({ isMode });
    });
  }

  enquireScreen = (cb) => {
    /* eslint-disable no-unused-expressions */
    enquire.register('only screen and (min-width: 320px) and (max-width: 1300px)', {
      match: () => {
        cb && cb(true);
      },
      unmatch: () => {
        cb && cb();
      },
    });
    /* eslint-enable no-unused-expressions */
  }

  render() {
    const children = [
      <Nav  id="nav_0_0" key="nav_0_0" isMode={this.state.isMode}/>,
      <Content0 id="content_0_0" key="content_0_0" isMode={this.state.isMode}/>,
      <Content1 id="content_2_0" key="content_2_0" isMode={this.state.isMode}/>,
      <Content2 id="content_3_0" style={{minHeight:'51vh'}} key="content_3_0" isMode={this.state.isMode}/>,
      <Content3 id="content_4_0" key="content_4_0" isMode={this.state.isMode}/>,
      <Footer id="footer_1_0"  key="footer_1_0" isMode={this.state.isMode}/>,
    ];
    return (
      <div className={style.templates_wrapper}>
        {children}
      </div>
    );
  }
}

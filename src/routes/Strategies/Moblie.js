import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator } from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import reqwest from 'reqwest';
const Item = List.Item;
const Brief = Item.Brief;
class Strategies extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
      pageCurrent:0,
      animating:false
		}
	}
	loadServer(params={} ) {
    // alert(params);
    let _data=[];
    let _this=this;
    this.setState({ animating: true });
    reqwest({
      url: window.host+'/api/strategies',
      method: 'get',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        page:params,
        pre:5
      },
      type: 'json',
    }).then((data) => {
      // alert(data.status);
      localStorage['regions']=JSON.stringify(data.regions);
      const pagination = { ...this.state.pagination };
      pagination.total = data.total;
      this.setState({
        animating: false,
        list:data.list,
        pagination:pagination.total,
      });
    }).fail( (err, msg) =>{
       console.log(1)
    })
  }
  componentWillMount(){
  	this.loadServer(1);
    if(JSON.parse(window.localStorage["setting"]).role=='admin'){
      window.sessionStorage['new']='/hfive/newstra';
      window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editstra","text":"编辑"}]);
      this.setState({aaa:'aa'})
    }
  }
  handelPageChage(e,tar){
    this.setState({pageCurrent:e})
    this.loadServer(e+1);
    // console.log(tar)
  }
  
	render(){
		const ItemList = this.state.list.map(function(data){
			return <Link key={data.id} onClick={()=>{window.sessionStorage["ism"]=JSON.stringify(data)}} to={{pathname:'/hfive/infostra'}}><Item
        arrow="horizontal"
        multipleLine
        onClick={() => {}}
        className={`${Reset.tel_list}`}
        platform="android"
      >
        主题:{data.title}<Brief>提成:{data.rate}/折扣:{data.discount}/返利:{data.rebate}</Brief>
        {data.start_at===null?undefined:<Brief>起止时间:{data.start_at.match(/\d+\-\d+\-\d+/)[0]}/{data.end_at.match(/\d+\-\d+\-\d+/)[0]}</Brief>}
      </Item></Link>
		})
		return(
			<div className="flex-container">
        <List renderHeader={() => '策略'}  className={`${Reset.tel_list_header}`}>
      	  {this.state.list.length<1?<Item
              arrow="horizontal"
              multipleLine
              onClick={() => {}}
              platform="android"
              className={`${style.dislike}`}
            >
              <Icon type={require('!svg-sprite!../../assets/Path/dislike.svg')} />
              <Brief>暂无数据</Brief>
           </Item>:ItemList}
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <Pagination onChange={this.handelPageChage.bind(this)} current={this.state.pageCurrent} style={{padding:'0.6rem 0.3rem'}} total={Math.ceil(this.state.pagination/5)} 
		      locale={{
		        prevText: (<div className={`${style.arrow_align}`}><Icon type="left" />上一页</div>),
		        nextText: (<div className={`${style.arrow_align}`}>下一页<Icon type="right" /></div>),
		      }} />
			</div>
		)
	}
}
export default connect()(Strategies)
import React from 'react';
import { connect } from 'dva';
import { Drawer, List, NavBar,Icon, InputItem, WhiteSpace ,Picker, TabBar,Pagination,ActivityIndicator,Card,TarBar} from 'antd-mobile';
import { createForm } from 'rc-form'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import reqwest from 'reqwest';
import {datevali_deta} from '../../utils/Validator.js'
const Item = List.Item;
const Brief = Item.Brief;
class StrategyLink extends React.Component{
  constructor(props){
    super(props);
    this.state={
      list:[],
      pageCurrent:0,
      animating:false,
      ism:JSON.parse(window.sessionStorage["ism"])
    }
  }
  componentWillMount(){
    // alert(localStorage['token'])
    // if(window.location.host=="172.16.0.249:8181"){
    //  // sessionStorage['token']="9aySPxzAmrO2xwg/PKFFKyOR/Tz3AB7OsmxH2kRUn/S2X3HFwtv386zl3gRB8nOkUdo4zUsqepccrCe1BgPrTA=="
    // }
    // if(JSON.parse(window.sessionStorage["setting"]).role=='admin'){
    	window.sessionStorage['menu_edit']=JSON.stringify([{"url":"/hfive/editstore","text":"编辑"}]);
    	this.setState({aaa:'aa'})
    // }
  }
  handelPageChage(e,tar){
    // this.setState({pageCurrent:e})
    // this.loadServer(e+1);
    // console.log(tar)
  }
  componentWillUnmount(){
    console.log(1);
    // window.sessionStorage.removeItem('bianji'); 
  }
  render(){
    const {ism} = this.state;
    return(
      <div className="flex-container">
        <List renderHeader={() => '策略信息'}  className={`${style.infor}`}>
          <Brief><span>主题:</span><span>{ism.title===''?'空':ism.title}</span></Brief>
          <Brief><span>开始时间:</span><span>{ism.start_at===null?'空':datevali_deta(ism.start_at)}</span></Brief>
          <Brief><span>截止时间:</span><span>{ism.end_at===null?'空':datevali_deta(ism.end_at)}</span></Brief>
          <Brief><span>地址:</span><span>{ism.province===''?'空':ism.province+ism.city+ism.area}</span></Brief>
          <Brief><span>提成:</span><span>{ism.rate}</span></Brief>
          <Brief><span>折扣:</span><span>{ism.discount}</span></Brief>
          <Brief><span>返利:</span><span>{ism.rebate}</span></Brief>
          <Brief><span>优先级:</span><span>{ism.pound===null?'空':ism.pound}</span></Brief>
          <ActivityIndicator
            toast
            text="正在加载"
            animating={this.state.animating}
          />
        </List>
        <List renderHeader={() => '基础信息'}  className={`${style.infor}`}>
          <Brief><span>创建时间:</span><span>{ism.created_at.match(/\d+\-\d+\-\d+/)[0]}</span></Brief>
          <Brief><span>修改时间:</span><span>{ism.updated_at.match(/\d+\-\d+\-\d+/)[0]}</span></Brief>
        </List>

      </div>
    )
  }
}
export default connect()(StrategyLink)
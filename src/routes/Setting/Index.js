import React from 'react';
import { connect } from 'dva';
import {Link} from 'dva/router'
import { Drawer, List, NavBar ,Icon,Modal,WhiteSpace,InputItem,Popup,Button,Card,Toast} from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../../style/App.less'
import NavStyle from '../../style/ResNav.less'
import Reset from '../../style/ResetInput.less'
// import PayStyle from './Pay.less'
// import reqwest from 'reqwest'
// import pingpp from 'pingpp-js'
import {reqwest_post,reqwest_get} from '../../utils/Reqwest.js';
const Item = List.Item;
class SetIndex extends React.Component{
	constructor(props){
		super(props);
		this.state={
			set:JSON.parse(window.localStorage['setting'])
		}
	}
	componentWillMount(){
		// this.getBankCard()
		window.sessionStorage.removeItem('bank')
	}
	render(){
		let _company = null;
		if(this.state.set.role=='admin'){
			_company =  (<Link to={{pathname:'/company_me'}}><Item
	        multipleLine
	        className={`${Reset.tel_list}`}
	        onClick={() => {}}
	        platform="android"
	        style={{borderBottom:'1px solid #eee'}}
	  	  >
	        企业资料设置
	      </Item>
	    </Link>)
		}
		return(
			<div style={{height:'100%'}}>
	 			<WhiteSpace size='xl' />
	 			<List style={{background:'#fff'}}  className={`${Reset.tel_list_header}`}>
	      	<Link  to={{pathname:'/set_me'}}><Item
			        multipleLine
			        className={`${Reset.tel_list}`}
			        onClick={() => {}}
			        platform="android"
			        style={{borderBottom:'1px solid #eee'}}
		    	  >
			        个人资料设置
			      </Item>
		      </Link>
		      {_company}
		      <Link to={{pathname:'/resetpass'}}><Item
			        multipleLine
			        className={`${Reset.tel_list}`}
			        onClick={() => {}}
			        platform="android"
			        style={{borderBottom:'1px solid #eee'}}
		    	  >
			        修改密码
			      </Item>
		      </Link>
	 			</List>
    </div>
		)
	}
}
export default connect()(SetIndex)
import React from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Drawer, List, NavBar,Icon, InputItem ,Picker, TabBar,Pagination ,Toast ,Button,NoticeBar,WhiteSpace} from 'antd-mobile';
import { createForm } from 'rc-form'
import style from '../../style/App.less'
import Reset from '../../style/ResetInput.less'
import NavStyle from '../../style/ResNav.less'
import {reqwest_get,reqwest_post} from '../../utils/Reqwest.js'

class ResetPassword extends React.Component{
	constructor(props){
		super(props);
		this.state={
      loading:false,
      disable:true
		}
	}
	handleSubmit(e){
		e.preventDefault();
    let _this =this;

    this.props.form.validateFields((err, values) => {
      console.log(values);
      if (!err) {
        _this.setState({loading:true})
        reqwest_post('/api/users/update_password',{access_token:window.localStorage['token'],old_password:values.oldpass,new_password:values.firstpass},(data) => {
            if(data.status=='success'){
              _this.setState({loading:false});
              // let set ={"mobile":data.current_user.mobile,"avatar":data.current_user.avatar,"avatar_url":data.current_user.avatar_url,"name":data.current_user.name,"role":data.current_user.role,"accout_name":data.current_user.account.name,"invit_url":data.current_user.account.invit_url}
                Toast.success('密码重置成功,请重新登录',1,()=>{

                  // window.location.hash=window.location.hash
                  window.localStorage.clear()
                  window.location.hash='/log';
                }
              )
              
            }else{
              _this.setState({loading:false});
              Toast.fail(data.msg)
            }
            
            
        })
      }
    });
	}
  componentWillMount(){
    console.log(history)
   
    // alert(window.location)
  }
  componentWillReceiveProps(next,props){
    // let _this = this;
    // console.log(next)
    // console.log(props)
    // this.props.form.validateFields((err, values) => {
    //   if(!err){
    //     _this.setState({disable:false})
    //   }
    // })
  }
	render(){
		 const { getFieldProps,getFieldError ,getFieldValue,setFields,getFieldsValue,getFieldsError } = this.props.form;
     let _falg=true;
     let _obj ={}
     // console.log(getFieldsValue())
     if(getFieldValue("oldpass")){
      // console.log(getFieldsValue()==typeof({}))
      for(let i in getFieldsError()){
        _falg =getFieldError(""+i)!=undefined||getFieldValue(''+i)==undefined?true:false;
        console.log(_falg)
      }
     }
		 return(
			<div>
        <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/set'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>} iconName={false} >修改密码</NavBar>
        <WhiteSpace size='xl' />
				<List >
          <InputItem
            className={`${Reset.tel_input}`}
            {...getFieldProps('oldpass',{
              rules:[{required:true,message:'请输入你的密码'}]
            })}
            error={!!getFieldError('oldpass')}
            onErrorClick={() => {
              alert(getFieldError('oldpass').join('、'));
            }}
            type="password"
            placeholder="****"
          >旧密码</InputItem>
          <InputItem
            className={`${Reset.tel_input}`}
            {...getFieldProps('firstpass',{
              rules: [{ required: true, message: '请输入密码',validator:(rule,value,callback)=>{
                if(!!value){
                  if(getFieldValue('secondpass')!==value&&!!getFieldValue('secondpass')){
                    setFields({
                      'secondpass':{
                        value:getFieldValue('secondpass'),
                        errors:[new Error('两次密码输入不一致')]
                      }
                    })
                    callback()
                  }else if(getFieldValue('secondpass')==value){
                    setFields({
                      'secondpass':{
                        value:getFieldValue('secondpass'),
                      }
                    })
                    callback()
                  }
                }else{
                  callback('请输入密码')
                }

              } }],
            })}
            error={!!getFieldError('firstpass')}
            onErrorClick={() => {
              alert(getFieldError('firstpass').join('、'));
            }}
            type="password"
            placeholder="****"
          >新密码</InputItem>
          <InputItem
            className={`${Reset.tel_input}`}
            {...getFieldProps('secondpass',{
              rules: [{ required: true,validator:(rule,value,callback)=>{
                    // console.log(value)
                if(!!value){
                  if(getFieldValue('firstpass')==value){
                    callback()
                  }else{
                    callback('两次输入密码需要一致')
                  }
                }else{
                  callback('请再次输入新密码')
                }
              } }],
            })}
            error={!!getFieldError('secondpass')}
            onErrorClick={() => {
              alert(getFieldError('secondpass').join('、'));
            }}
            type="password"
            placeholder="****"
          >密码</InputItem>
        </List>
        <WhiteSpace size='lg' />
        <Button disabled={_falg} className={`${Reset.btn_moblie} ${Reset.tel_btn}`} onClick={this.handleSubmit.bind(this)}  type='primary'>修改密码</Button>
			</div>
	 	)
		
	}
}
const ResetPass = createForm()(ResetPassword);

export default connect()(ResetPass)
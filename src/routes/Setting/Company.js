import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace,ImagePicker,ActionSheet,} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import setStyle from '../../style/Set.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
import NavStyle from '../../style/ResNav.less'
import {role_case} from '../../utils/New.js';
const Brief =  List.Item.Brief;
const iconList = [
  { icon: <img src="https://zos.alipayobjects.com/rmsportal/WmEzpOsElbbvgmrexFSH.png" alt="icon" />, title: '发送给朋友' },
  { icon: <img src="https://zos.alipayobjects.com/rmsportal/HssPJKvrjEByyVWJIFwl.png" alt="icon" />, title: '新浪微博' },
  { icon: <img src="https://zos.alipayobjects.com/rmsportal/HCGowLrLFMFglxRAKjWd.png" alt="icon" />, title: '生活圈' },
  { icon: <img src="https://zos.alipayobjects.com/rmsportal/LeZNKxCTkLHDWsjFfqqn.png" alt="icon" />, title: '微信好友' },
  { icon: <img src="https://zos.alipayobjects.com/rmsportal/YHHFcpGxlvQIqCAvZdbw.png" alt="icon" />, title: 'QQ' },
  // { icon: <Icon type={require('./refresh.svg')} />, title: '刷新' },
  // { icon: <Icon type={require('./link.svg')} />, title: '链接' },
  // { icon: <Icon type={require('./complaints.svg')} />, title: '投诉' },
];
class CompanySet extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
			set:JSON.parse(window.localStorage['setting']),
			files:[],
			filesList:[],
      flag:0,
      loading:false,
      clicked2: 'none',
		}
	}
	loadServer (params = {}) {
    // console.log('params:', params);
    let _data=[];
    let _user;

    const {files,flag,set} = this.state;
    this.setState({loading:true})
    let _set = set
    console.log(files)
    flag==1?_user={address:params.address,avatar:files[0].url}:_user={address:params.address}
    reqwest({
      url: window.host+'/api/accounts/update_me',
      method: 'post',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        account:_user
      },
      type: 'json',
    }).then((data) => {
      // let set ={"mobile":data.current_user.mobile,"avatar":data.current_user.avatar,"avatar_url":data.current_user.avatar_url,"name":data.current_user.name,"role":data.current_user.role,"accout_name":data.current_user.account.name,"invit_url":data.current_user.account.invit_url};
      if(data.status=='success'){
        _set.accout_address=data.account.address;
      	Toast.success('更新成功', 1.5,()=>{
          window.localStorage['setting']=JSON.stringify(_set);
     		  window.location.hash='/hfive/set';
      	});
      }else{
      	Toast.fail(data.msg)
      }
    }).fail( (err, msg) =>{
    	 Toast.offline('网络连接失败!!!', 0.5);
       console.log(1)
    })
    this.setState({loading:false})
  }

	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
        console.log(values.province)
      if (!err  ) {
        console.log('Received values of form: ', values);
      	_this.loadServer(values)
      }
    });
	}
	onChange (files, type, index) {
    console.log(files, type, index);
    this.setState({
      flag:1,
      files,
    });
  }
  handleImageClick(index,fs){
  	this.onChange.bind(this);
  }
	componentWillMount(){
		let _regions=[];
    let ava =JSON.parse(window.localStorage['setting']).avatar_url==null?"https://images.chuanggj.com/uploads/user/headimg/80/1490154626_%E5%BB%BA%E7%AD%913.png":JSON.parse(window.localStorage['setting']).avatar_url
		this.setState({
			files:[{uid: -1,
      name: 'xxx.png',
      url:ava}]
		})
	}
  showAction(){
    // let url = document.getElementById('invit_url');
    document.getElementById('invit_url').select(); // 选择对象

    document.execCommand("Copy");
    alert("已复制好，可贴粘。");
    // const icons = [[...iconList],];
    // ActionSheet.showShareActionSheetWithOptions({
    //   options: icons,
    //   // title: '标题',
    //   message: '我是描述我是描述',
    //   className: 'my-action-sheet',
    // },
    // (buttonIndex, rowIndex) => {
    //   this.setState({ clicked2: buttonIndex > -1 ? icons[rowIndex][buttonIndex].title : 'cancel' });
    // });
  }
	render(){
		console.log(this.state.list)
			const { set} =this.state;
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
		 return(
			<div className={`${style.layout}`}>
        <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/set'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>} iconName={false} >设置个人资料</NavBar>
				
     	  <WhiteSpace size='lg' />
				<List >
          <InputItem className={`${Reset.tel_input} ${Reset.tel_input_disanled}`} value={set.invit_url} id='invit_url'>
            
          </InputItem>
          <List.Item className={`${Reset.tel_input}`}>
            <div style={{ margin: '0.15rem 0' }}>
              <Button onClick={this.showAction.bind(this)}>点击按钮复制链接</Button>
            </div>
          </List.Item>

          <InputItem
          	className={`${Reset.tel_input} ${Reset.tel_input_disanled}`}
	          clear
	          error={!!getFieldError('name')}
	          onErrorClick={() => {
	            alert(getFieldError('name').join('、'));
	          }}
            {...getFieldProps('name',{
            	initialValue:set.accout_name,
            	rules:[{required:true,message:'企业名称'}]
            })}
            placeholder="企业名称"
            disabled={true}
          >企业名称</InputItem>
          <InputItem
            className={`${Reset.tel_input}`}
            clear
            error={!!getFieldError('address')}
            onErrorClick={() => {
              alert(getFieldError('address').join('、'));
            }}
            {...getFieldProps('address',{
              initialValue:set.accout_address,
              rules:[{required:true,message:'企业地址'}]
            })}
            placeholder="企业地址"
          >企业地址</InputItem>
        </List>
        <WhiteSpace size='xl' />
        <Button loading={this.state.loading} type='primary' className={`${Reset.btn_moblie} ${Reset.tel_btn}`}  onClick={this.handleSubmit.bind(this)} >保存</Button>
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}
// var Url2=document.getElementById("address");
// Url2.select(); // 选择对象
// document.execCommand("Copy"); // 执行浏览器复制命令
// alert("已复制好，可贴粘。");
// <List className={`${setStyle.set_header}`}>
            // <ImagePicker
            // className={`${setStyle.cha}`}
        //       files={this.state.files}
        //       onChange={this.onChange.bind(this)}
        //       onImageClick={this.handleImageClick.bind(this)}
        //       selectable={this.state.files.length < 1}
        //     />
        //     <span className={`${setStyle.span}`}>手机号码：{set.mobile}</span>
        //     <span className={`${setStyle.span}`}>角色：{role_case[set.role]}</span>
        //       <ImagePicker
        //      files={this.state.filesList}
            //  className={`${setStyle.set_img}`}
            //  onChange={this.onChange.bind(this)}
        //       selectable={this.state.filesList.length < 1}
        //     />
        // </List>
const CompanySetForm = createForm()(CompanySet);
export default connect()(CompanySetForm)
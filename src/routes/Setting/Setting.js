import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace,ImagePicker} from 'antd-mobile';
import { createForm } from 'rc-form';
import Reset from '../../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../../style/App.less'
import setStyle from '../../style/Set.less'
import reqwest from 'reqwest';
import {numvail,selectvail} from '../../utils/Validator.js'
import options from '../../models/CityData.js'
import PropTypes from 'prop-types';
import NavStyle from '../../style/ResNav.less'
import {role_case} from '../../utils/New.js';
const Brief =  List.Item.Brief;
class CustomerCreate extends React.Component{
		constructor(props){
		super(props);
		this.state={
			reginos:[],
			disable:true,
			set:JSON.parse(window.localStorage['setting']),
			files:[],
			filesList:[],
      flag:0
		}
	}
	loadServer (params = {}) {
    // console.log('params:', params);
    let _data=[];
    let _user;
    const {files,flag} = this.state;
    console.log(files)
    flag==1?_user={name:params.name,avatar:files[0].url}:_user={name:params.name}
    // return
    reqwest({
      url: window.host+'/api/users/update_me',
      method: 'post',
      crossOrigin: true,
      data: {
        access_token:localStorage['token'],
        user:_user
      },
      type: 'json',
    }).then((data) => {
      let set ={"mobile":data.current_user.mobile,"avatar":data.current_user.avatar,"avatar_url":data.current_user.avatar_url,"name":data.current_user.name,"role":data.current_user.role,"accout_name":data.current_user.account.name,"invit_url":data.current_user.account.invit_url};
      if(data.status=='success'){
      	Toast.success('更新成功', 1.5,()=>{
          window.localStorage['setting']=JSON.stringify(set);
     		  window.location.hash='/hfive/set';
      	});
      }else{
      	Toast.fail(data.msg)
      }
    }).fail( (err, msg) =>{
    	 Toast.offline('网络连接失败!!!', 0.5);
       console.log(1)
    })
  }

	handleSubmit(e){
		e.preventDefault();
    let _this=this;
    // this.props.form.getFieldError('cha_name')
    this.props.form.validateFields((err, values) => {
        // console.log(this.state.payMethods.match(/\d+/g));
        // let chaname= values.chaname!==undefined?values.chaname.match(/\d+/g):null;
        console.log(values.province)
      if (!err  ) {
        console.log('Received values of form: ', values);
      	_this.loadServer({
      		name:values.name,mobile:values.phone,province:values.province,address:values.address,remark:values.remark
      	})
      }
    });
	}
	onChange (files, type, index) {
    console.log(files, type, index);
    this.setState({
      flag:1,
      files,
    });
  }
  handleImageClick(index,fs){
  	this.onChange.bind(this);
  }
	componentWillMount(){
		let _regions=[];
    let ava =JSON.parse(window.localStorage['setting']).avatar_url==null?"https://images.chuanggj.com/uploads/user/headimg/80/1490154626_%E5%BB%BA%E7%AD%913.png":JSON.parse(window.localStorage['setting']).avatar_url
		this.setState({
			files:[{uid: -1,
      name: 'xxx.png',
      url:ava}]
		})
	}
	render(){
		console.log(this.state.list)
			const { set} =this.state;
		 const { getFieldProps , getFieldError} = this.props.form;
		 const _this =this;
		 return(
			<div className={`${style.layout}`}>
        <NavBar  style={{height:'1rem',background:'#3dbd7d',fontSize:'0.36rem' }} className={`${NavStyle.tel_nav}`} leftContent={<Link className={`${style.white}`} to={{pathname:'/hfive/set'}}><Icon type="left"  style={{marginBottom:'-0.1rem'}}  />返回</Link>} iconName={false} >设置个人资料</NavBar>
				<List className={`${setStyle.set_header}`}>
						<ImagePicker
						className={`${setStyle.cha}`}
		          files={this.state.files}
		          onChange={this.onChange.bind(this)}
		          onImageClick={this.handleImageClick.bind(this)}
		          selectable={this.state.files.length < 1}
		        />
		        <span className={`${setStyle.span}`}>手机号码：{set.mobile}</span>
		        <span className={`${setStyle.span}`}>角色：{role_case[set.role]}</span>
		       	 <ImagePicker
		        	files={this.state.filesList}
							className={`${setStyle.set_img}`}
							onChange={this.onChange.bind(this)}
		          selectable={this.state.filesList.length < 1}
		        />
     	  </List>
     	  <WhiteSpace size='lg' />
				<List >
          <InputItem
          	className={`${Reset.tel_input}`}
	          clear
	          error={!!getFieldError('name')}
	          onErrorClick={() => {
	            alert(getFieldError('name').join('、'));
	          }}
            {...getFieldProps('name',{
            	initialValue:set.name,
            	rules:[{required:true,message:'姓名'}]
            })}
            placeholder="姓名"
          >姓名</InputItem>
        </List>
        <WhiteSpace size='xl' />
        <Button type='primary' className={`${Reset.btn_moblie} ${Reset.tel_btn}`}  onClick={this.handleSubmit.bind(this)} >保存</Button>
        <WhiteSpace size="sm" />
			</div>
	 	)
		
	}
}
         //  <Picker   extra={<span>地址</span>} style={{textAlign:'left'}} clear
          //    error={!!getFieldError('province')}
           //    onErrorClick={() => {
           //      alert(getFieldError('province').join('、'));
           //    }}
          //     placeholder="选择地址"
           //    onOk={(e)=>{this.setState({disable:false});console.log(e)}}
           //    {...getFieldProps('province',{
           //     rules:[{required:true,message:'请选择地址'}]
           //    })}  title="选择地址" data={options} >
          //   <List.Item arrow="horizontal"
          //    className={`${Reset.list_extra} ${Reset.tel_input}`}
          //   >选择地址</List.Item>
          // </Picker>
         //  <InputItem
          //   clear
          //   className={`${Reset.tel_input}`}
          //   error={!!getFieldError('address')}
          //   onErrorClick={() => {
          //     alert(getFieldError('address').join('、'));
          //   }}
         //    {...getFieldProps('address',{
         //     rules:[{required:true,message:'请输入详细地址'}]
         //    })}
         //    placeholder='详细地址'
         //  >详细地址</InputItem>
   // <List.Item>
          // 		手机号码:&ensp;&ensp;{set.mobile}
          // </List.Item>
          // <List.Item>
          // 		角色:&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;{set.role}
          // </List.Item>
const CustomerForm = createForm()(CustomerCreate);
export default connect()(CustomerForm)
import React from 'react';
import {connect} from 'dva';
import PropTypes from 'prop-types';
import {DatePicker,Icon, } from 'antd';
import style from '../style/ResetInput.less';
import inStyle from '../style/Info.less';
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
function range(start, end) {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
}

function disabledDate(current) {
  // can not select days before today and today
  // debugger;
  // console.log(current)
  // let _date = new Date()
  return current && current.valueOf() < Date.now();
}
function disabledDateTime() {
  return {
    disabledHours: () => range(0, 24).splice(4, 20),
    disabledMinutes: () => range(30, 60),
    disabledSeconds: () => [55, 56],
  };
}
function disabledRangeTime(_, type) {
  if (type === 'start') {
    return {
      disabledHours: () => range(0, 60).splice(4, 20),
      disabledMinutes: () => range(30, 60),
      disabledSeconds: () => [55, 56],
    };
  }
  return {
    disabledHours: () => range(0, 60).splice(20, 4),
    disabledMinutes: () => range(0, 31),
    disabledSeconds: () => [55, 56],
  };
}
class DateSelect extends React.Component{
	render(){
		return(
			<DatePicker
				style={{width:'100%',borderBottom:'1px solid #d9d9d9'}}
	      format="YYYY-MM-DD HH:mm:ss"
	      disabledDate={disabledDate}
	      showTime
	    />
		)
	}
}
export default connect()(DateSelect);
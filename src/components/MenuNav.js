import React from 'react';
// import ReactDOM from 'react-dom'
// import { connect } from 'dva';
import { Menu, Icon } from 'antd';
import PropTypes from 'prop-types';
// import $ from "jquery";
// import 'whatwg-fetch';
// import promise from 'es6-promise';
import { Link } from 'react-router';
import reqwest from 'reqwest';
import {connect} from 'dva';
let SubMenu = Menu.SubMenu;
// promise.polyfill();
import QueueAnim from 'rc-queue-anim';
class Sider extends React.Component{
	loadCommentsFromServer(){
		let _this=this;
		window.sessionStorage.setItem('nav','[{"title":"货运管理","icon":"home","menu":[{"attributes":{"menu_name":"运单管理","url":"/waybills"}},{"attributes":{"menu_name":"运输管理","url":"/transports"}},{"attributes":{"menu_name":"车辆位置","url":"/vehicles/list"}},{"attributes":{"menu_name":"运输时效","url":"/time_limits"}},{"attributes":{"menu_name":"历史运单","url":"/post_waybills"}},{"attributes":{"menu_name":"历史报表","url":"/waybills_report"}},{"attributes":{"menu_name":"价格查询","url":"/line_prices/search_price"}},{"attributes":{"menu_name":"订单管理","url":"/orders"}}]},{"title":"账务管理","icon":"pay-circle-o","menu":[{"attributes":{"menu_name":"零钱提现支付管理","url":"/not_commit_payment"}},{"attributes":{"menu_name":"支付管理","url":"/pre_payments"}},{"attributes":{"menu_name":"银行流水报表","url":"/pays"}},{"attributes":{"menu_name":"支付报表","url":"/payments"}},{"attributes":{"menu_name":"备付金","url":"/payments/reserve"}}]},{"title":"系统管理","icon":"setting","menu":[{"attributes":{"menu_name":"承运商管理","url":"/accounts/relation_list"}},{"attributes":{"menu_name":"价格管理","url":"/line_prices"}},{"attributes":{"menu_name":"地址管理","url":"/addresses"}},{"attributes":{"menu_name":"产品管理","url":"/products"}},{"attributes":{"menu_name":"产品分类","url":"/product_categories"}}]},{"title":"平台管理","icon":"bars","menu":[{"attributes":{"menu_name":"账户管理","url":"/accounts"}},{"attributes":{"menu_name":"用户管理","url":"/users"}},{"attributes":{"menu_name":"业务数据报表","url":"/platform/posts"}},{"attributes":{"menu_name":"短信模板管理","url":"/sms_templates"}},{"attributes":{"menu_name":"车型管理","url":"/vehicle_types"}},{"attributes":{"menu_name":"车辆类别管理","url":"/vehicle_categories"}},{"attributes":{"menu_name":"车辆管理","url":"/vehicles"}},{"attributes":{"menu_name":"运输线路管理","url":"/transport_lines"}},{"attributes":{"menu_name":"产品类型管理","url":"/product_types"}},{"attributes":{"menu_name":"版本管理","url":"/configs"}},{"attributes":{"menu_name":"城市分级管理","url":"/counties"}},{"attributes":{"menu_name":"承运商区域管理","url":"/carrier_regions"}},{"attributes":{"menu_name":"银行卡标识管理","url":"/bank_identifiers"}}]}]');
		if(window.sessionStorage.getItem('nav') == null){
			reqwest({
			    url: 'http://192.168.1.60:3008/menus',
			    type:"json",
			    crossOrigin: true ,
			    method: 'get',
			    success: function (response) {
			    	console.log(response);
			    	window.sessionStorage.setItem('nav',JSON.stringify(response.data));
			   		_this.setState({nav:response.data})
			    },
			    error:function(err){
			    	
			    }
			})	
		}else{
			_this.setState({nav:JSON.parse(window.sessionStorage.getItem('nav'))})
		}
		
	}
	componentWillMount(){
		this.loadCommentsFromServer();

	}
	constructor(props){
		super(props);
		this.state={
			current:this.props.location.query.current,
			openKeys:[this.props.location.query.openKeys],
			ele:[],
			nav:[],
			navCode:[{title:'主页',url:'/home'},
			{title:'客户',url:'/customer'},
			{title:'动态',url:'/dynamic'},
			{title:'线索',url:'/clue'},
			{title:'报表',url:'/datatable'},
			{title:'商品',url:'/commodity'},
			{title:'渠道/门店',url:'/channel'},
			{title:'销售订单',url:'/saler'},
			{title:'策略',url:'/strategy'},]
		}
	}
	handleClick(e){
		// console.log('click ', e);
		this.setState({ current: e.key });
	}
	onOpenChange(openKeys){
		if(!!window.find){
			var latestOpenKey = openKeys.find(key => !(this.state.openKeys.indexOf(key) > -1));
			// console.log(latestOpenKey);
		}else{
			Array.prototype.find = function (func) {
	            var temp = [];
	            for (var i = 0; i < this.length; i++) {
	                if (func(this[i])) {
	                    temp[temp.length] = this[i];
	                }
	            }
	            return temp;
	        }
			var latestOpenKey = openKeys.find(key => !(this.state.openKeys.indexOf(key) > -1));
		}
    	this.setState({ openKeys: this.getKeyPath(latestOpenKey) });
	}
	getKeyPath(key) {
		let map = {};
		this.state.nav.map(function(o,i){
			i++
			map['sub'+i] = ['sub'+i];
		});
	    return map[key] || [];
	}
	render(){
		let _math =0;
		let _text = "";
		let count = 0;
		// {data.menu.map(function(aaa){
				// 	count++;
				// 	return <Menu.Item key={count}><Link style={{paddingLeft:22}} to={{pathname:"/home"+aaa.url,query:{'current':count,'openKeys':_text} }}>{aaa.attributes.menu_name}</Link></Menu.Item>
				// })}
		let _commentNodes = this.state.navCode.map(function(data){
			_text="sub"+ ++_math;
			// console.log(_math);
			return <SubMenu key={_text} title={<span><Icon type='/bars' /><span>{data.title}</span></span>}>
				
			</SubMenu>
		});
		return(
			<QueueAnim type='bottom'>
			<Menu key='a' mode="inline" openKeys={this.state.openKeys} 
			selectedKeys={[this.state.current]} style={{ width: '100%' ,borderLeft:'1px solid rgb(233,233,233)'}}
			onOpenChange={this.onOpenChange} onClick={this.handleClick}>
				{_commentNodes}
			</Menu>
			</QueueAnim>
		)
	}
}
export default Sider;
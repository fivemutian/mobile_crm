import React from 'react';
import { connect } from 'dva';
import {  Icon,WhiteSpace,Popover} from 'antd-mobile';
import Reset from '../style/ResetInput.less';
import { Link } from 'dva/router';
import style from '../style/App.less'
import PropTypes from 'prop-types';
import {New_case} from '../utils/New.js';
const Item = Popover.Item;
class Right extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:null,
			visible:false,
		}
	}
	componentWillMount(){
		// let menu_edit=window.sessionStorage['menu_edit']==undefined?[]:JSON.parse(window.sessionStorage['menu_edit']);
    console.log(New_case[this.props.url])
		// this.setState({list:menu_edit})
	}
	onSelect = (e) => {
    // console.log(e);
    this.setState({
      visible: false,
      // selected: opt.props.value,
    });
  }
  handleVisibleChange = (visible) => {
    console.log(visible);
    // visible.stopPropagation();

    this.setState({
      visible,
    });
  }
	render(){
		// console.log(this.state.list)
		let _this=this;
		let offsetX = -10; // just for pc demo
    if (/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)) {
      offsetX = -26;
    }
    let _text =window.sessionStorage['new']!='/hfive/newapply'?'新建':'关联'
		return(
			<Popover mask
        overlayClassName="fortest"
        overlayStyle={{ color: 'currentColor' }}
        visible={this.state.visible}
        overlay={[(<Item  className={`${Reset.tel_popover}`}><Link style={{fontSize:'0.34rem'}} to={{pathname:New_case[this.props.url].url}}>{New_case[this.props.url].text}</Link></Item>)]}
        align={{
          overflow: { adjustY: 0, adjustX: 0 },
          offset: [offsetX, 15],
        }}
        onVisibleChange={this.handleVisibleChange}
        onSelect={this.onSelect}
      >
        <div style={{
          height: '100%',
          padding: '0 0.3rem',
          marginRight: '-0.3rem',
          display: 'flex',
          alignItems: 'center',
        }}
        >
          <Icon  type={require('!svg-sprite!../assets/Path/plus.svg')} />
        </div>
      </Popover>
		)
	}
}
export default connect()(Right)
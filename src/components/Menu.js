import React from 'react';
import { connect } from 'dva';
import { Picker,Drawer, List, NavBar,Icon, InputItem , TabBar,Pagination ,Toast ,Button,TextareaItem,WhiteSpace,Popover} from 'antd-mobile';
import Reset from '../style/ResetInput.less'
import { Link } from 'dva/router';
import style from '../style/App.less'
import PropTypes from 'prop-types';
import reqwest from 'reqwest'
import {New_edit} from '../utils/New.js';
const Item = Popover.Item;
let flag=0;
class Right extends React.Component{
	constructor(props){
		super(props);
		this.state={
			list:[],
			visible:false,
      // flag:0
		}
	}
	componentWillMount(){
		// let menu_edit=window.sessionStorage['menu_edit']==undefined?[]:JSON.parse(window.sessionStorage['menu_edit']);
		// this.setState({list:menu_edit})
    flag=0;
	}
	onSelect = (opt) => {
    // console.log(opt.props.value);
    this.setState({
      visible: false,
      selected: opt.props.value,
    });
  }
  handleVisibleChange(visible){
    this.setState({
      visible,
    });
  }
  onDelete(e,_url){
    // console.log(e)
    // return
    // this.setState({flag:1});
    flag=1;
    if(flag==1){
      reqwest({
        url: window.host+e+JSON.parse(window.sessionStorage['ism']).id,
        method: 'delete',
        crossOrigin: true,
        data: {
          access_token:localStorage['token'],
          // access_token:'1D34Z4i+CRE5DkYEBrpbhrigA+QU2lzIwEVqALrVmhQnPXO/THJGQvAPhLkwjO6U/czFYRNBoSwE9k8EQEA3RQ==',
        },
        type: 'json',
      }).then((data) => {
        console.log(data);
        // const pagination = { ...this.state.pagination };
        // _this.fetch();
        if(data.status=='success'){
          Toast.success('删除成功',0.6,()=>{
            window.location.hash=_url;
          })
        }else{
          // message
          Toast.fail(data.msg,0.3)
        }

      // Read total count from server
      // pagination.total = data.totalCount;
      });
    }
  }
	render(){
		console.log(this.state.list)
		let _this=this;
		let offsetX = -10; // just for pc demo
    if (/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)) {
      offsetX = -26;
    }
		const list = New_edit[this.props.location].map(function(data,index,obj){
			if(data.text=='删除'){
				return <Item  style={{padding:'0 0.2rem',fontSize:'0.34rem',textAlign:'center',}} className={`${Reset.tel_popover}`} key={index} value="scan"  data-seed="logId"><a onClick={()=>_this.onDelete(data.api,data.url)}>删除</a></Item>
			}else{
				return <Item style={{padding:'0 0.2rem',textAlign:'center',fontSize:'0.34rem'}} className={`${Reset.tel_popover}`} value="scan"  data-seed="logId"><Link   key={index}  to={{pathname:data.url,query:{ism:_this.props.location.ism}}}>编辑</Link></Item>
			}
		})
		return(
			<Popover mask
        overlayStyle={{ color: 'currentColor' }}
        visible={this.state.visible}
        overlay={[list
        ]}
        align={{
          overflow: { adjustY: 0, adjustX: 0 },
          offset: [offsetX, 15],
        }}
        onVisibleChange={this.handleVisibleChange.bind(this)}
        onSelect={this.onSelect}
      >
        <div style={{
          height: '100%',
          padding: '0 0.3rem',
          marginRight: '-0.3rem',
          display: 'flex',
          alignItems: 'center',
        }}
        >
          <Icon type={require('!svg-sprite!../assets/Path/plus.svg')} />
        </div>
      </Popover>
		)
	}
}
export default connect()(Right)
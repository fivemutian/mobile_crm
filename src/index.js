import dva from 'dva';
import './index.css';
import enquire from 'enquire.js';
if(window.location.host == "localhost:8181"||window.location.host == "172.16.0.48:8181"){
  window.host='';
  // window.host='http://api.salesgj.com';
}else if(window.location.host == "localhost:8111" ||window.location.host == "172.16.0.48:8111"){
  window.host='http://192.168.0.164:7200';
  // window.host='http://api.salesgj.com';
}else if(window.location.host=="192.168.0.164:7201"){
  window.host='http://192.168.0.164:7200';
}else{
  window.host='https://api.salesgj.com';
}
let lever = null;
let u = navigator.userAgent;
let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; 
let _hash = window.location.hash;
// &&navigator.platform!="MacIntel"
let isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
var _date = new Date();
// wx.config({
//   appid:'wx1211e96e6c279899',
//   timestamp:_date.getTime(),
//   nonceStr:'iujessica',
//    signature: '',
// })

function enquireScreen(cb){
  /* eslint-disable no-unused-expressions */
  enquire.register('only screen and (min-width: 320px) and (max-width: 767px)', {
    match: () => {
      cb && cb(true);
    },
    unmatch: () => {
      cb && cb();
    },
  });
  /* eslint-enable no-unused-expressions */
}

enquireScreen((isMode) => {
  lever=isMode;
});

if(lever){
  !function(e){function t(a){if(i[a])return i[a].exports;var n=i[a]={exports:{},id:a,loaded:!1};return e[a].call(n.exports,n,n.exports,t),n.loaded=!0,n.exports}var i={};return t.m=e,t.c=i,t.p="",t(0)}([function(e,t){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var i=window;t["default"]=i.flex=function(e,t){var a=e||100,n=t||1,r=i.document,o=navigator.userAgent,d=o.match(/Android[\S\s]+AppleWebkit\/(\d{3})/i),l=o.match(/U3\/((\d+|\.){5,})/i),c=l&&parseInt(l[1].split(".").join(""),10)>=80,p=navigator.appVersion.match(/(iphone|ipad|ipod)/gi),s=i.devicePixelRatio||1;p||d&&d[1]>534||c||(s=1);var u=1/s,m=r.querySelector('meta[name="viewport"]');m||(m=r.createElement("meta"),m.setAttribute("name","viewport"),r.head.appendChild(m)),m.setAttribute("content","width=device-width,user-scalable=no,initial-scale="+u+",maximum-scale="+u+",minimum-scale="+u),r.documentElement.style.fontSize=a/2*s*n+"px"},e.exports=t["default"]}]);
  flex(100, 1);
}
// if(lever){
// 	// window.location.hash=window.location.hash.replace('app','hfive')
// }else{
// 	window.location.hash=window.location.hash.replace('hfive','app')
// }
// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/example'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');

import React from 'react';
import { Router, Route,IndexRoute } from 'dva/router';
// import IndexPage from './routes/IndexPage';
const loginRequireAuth = function(nextState,replace){
    if(!localStorage['token']){
        replace({
            pathname: '/log',
            state: { nextPathname: nextState.location.pathname }
        });
    }
}

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Route path='/' >	
        <IndexRoute component={require('./routes/IndexPage/index.jsx')}  />
      </Route>
      <Route path='lef'>
      </Route>
      <Route path="hfive" component={require('./routes/App.js')} onEnter={loginRequireAuth}>
        <Route path='home' component={require('./routes/DashBord/Index.js')} />
      	<Route path='customers' component={require('./routes/Customer/Moblie.js')} />
      	<Route path='infocus' component={require('./routes/Customer/InforMation.js')} />
      	<Route path='newcus' component={require('./routes/Customer/Create.js')} />
      	<Route path='editcus' component={require('./routes/Customer/Edit.js')} />

      	<Route path='clues' component={require('./routes/Clues/Moblie.js')} />
      	<Route path='infocl' component={require('./routes/Clues/InforMation.js')} />
      	<Route path='newcl' component={require('./routes/Clues/Create.js')} />
      	<Route path='editcl' component={require('./routes/Clues/Edit.js')} />

      	<Route path="orders" component={require('./routes/Orders/Moblie.js')} />
      	<Route path="inforder" component={require('./routes/Orders/InforMation.js')} />
      	<Route path='newor' component={require('./routes/Orders/Create.js')} />
      	<Route path='listacct' component={require('./routes/Orders/Acct/List.js')} />
        <Route path='acctor' component={require('./routes/Orders/Acct/Index.js')} />

      	<Route path="stores" component={require('./routes/Stores/Moblie.js')} />
      	<Route path='newsroes' component={require('./routes/Stores/Create.js')} />
        <Route path="infosroes" component={require('./routes/Stores/InforMation.js')} />
        <Route path="editsroes" component={require('./routes/Stores/Edit.js')} />

      	<Route path="users" component={require('./routes/Users/Moblie.js')} />
        <Route path="newuse" component={require('./routes/Users/Create.js')} />
        <Route path="infouse" component={require('./routes/Users/InforMation.js')} />
        <Route path="edituse" component={require('./routes/Users/Edit.js')} />

      	<Route path="strategies" component={require('./routes/Strategies/Moblie.js')} />
        <Route path="infostra" component={require('./routes/Strategies/InforMation.js')} />
        <Route path="newstra" component={require('./routes/Strategies/Create.js')} />
        <Route path="editstra" component={require('./routes/Strategies/Edit.js')} />

      	<Route path="account" component={require('./routes/Companies/Account.js')} />
        <Route path='company' component={require('./routes/Companies/Company.js')} />
        <Route path='dealer' component={require('./routes/Companies/Delaer.js')} />
        <Route path='newapply' component={require('./routes/Companies/Apply.js')} />
        <Route path='newan' component={require('./routes/Companies/New.js')} />

        <Route path="pay" component={require('./routes/Pay/Pay.js')} />
        <Route path='set' component={require('./routes/Setting/Index.js')} />
      </Route>
      <Route path="bank"  onEnter={loginRequireAuth}>
         <Route path="list" component={require('./routes/Pay/Bank.js')} />
         <Route path="new" component={require('./routes/Pay/BankCreat.js')} />
         <Route path="edit" component={require('./routes/Pay/BankEdit.js')} />
      </Route>

      <Route path='set_me' component={require('./routes/Setting/Setting.js')} />
      <Route path="company_me" component={require('./routes/Setting/Company.js')} />
      <Route path="resetpass" component={require('./routes/Setting/ResetPassword.js')} hist={history} />

      <Route path="recharge" component={require('./routes/Pay/Recharge.js')} />
      <Route path="withdra" component={require('./routes/Pay/Withdrawals.js')} />
      <Route path="pay/status/success" component={require('./routes/Pay/Success.js')} />
      <Route path='log' component={require('./routes/Login/AppLogin.js')} />
      <Route path='reg' component={require('./routes/Login/Reg.js')} />
    </Router>
  );
}

export default RouterConfig;
